//
//  IntroFlowViewController.swift
//  Belle
//
//  Created by Steve on 4/6/19.
//  Copyright © 2019 Belle. All rights reserved.
//

import UIKit

class IntroFlowViewController: UIViewController {

	@IBAction func unwindToIntroVCFromConnect(segue:UIStoryboardSegue) { }
	
	@IBOutlet weak var backButton: UIButton!
	
	@IBOutlet weak var startSavingButton: UIButton!
	
	@IBOutlet weak var loginButton: UIButton!
	
	@IBOutlet weak var scrollView: UIScrollView!
	
	@IBOutlet weak var pageControl: UIPageControl!
	
	@IBAction func backPressed(_ sender: Any) {
		self.navigationController?.popViewController(animated: true)
	}
	@IBAction func startSavingPressed(_ sender: Any) {
		let signupViewController = storyboard?.instantiateViewController(withIdentifier: "signUpViewController") as! SignUpViewController
		signupViewController.fromIntroFlow = true
		self.navigationController?.pushViewController(signupViewController, animated: true)
	}
	
	@IBAction func loginPressed(_ sender: Any) {
		let loginViewController = storyboard?.instantiateViewController(withIdentifier: "loginViewController") as! LoginViewController
		self.navigationController?.pushViewController(loginViewController, animated: true)
	}
	
	var slides:[Slide] = [];
	
	override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
		scrollView.delegate = self
		
		slides = createSlides()
		setupSlideScrollView(slides: slides)
		
		pageControl.numberOfPages = slides.count
		pageControl.currentPage = 0
		
		view.bringSubviewToFront(pageControl)
		
		startSavingButton.layer.cornerRadius = 10
		startSavingButton.backgroundColor = UIColor.belleColor()
		
		loginButton.setTitleColor(UIColor.belleColor(), for: .normal)  
		
		if #available(iOS 11, *) {
			scrollView.contentInsetAdjustmentBehavior = .never
		}
    }
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		// the custom library works better on viewDidAppear
		pageControl.customPageControl(dotFillColor: UIColor.belleColor(), dotBorderColor: UIColor.belleColor(), dotBorderWidth: 1)
		
	}
	
	
	func setupSlideScrollView(slides : [Slide]) {
		scrollView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
		scrollView.contentSize = CGSize(width: view.frame.width * CGFloat(slides.count), height: view.frame.height)
		scrollView.isPagingEnabled = true
		
		for i in 0 ..< slides.count {
			slides[i].frame = CGRect(x: view.frame.width * CGFloat(i), y: 0, width: view.frame.width, height: view.frame.height)
			scrollView.addSubview(slides[i])
		}
	}
	
	func createSlides() -> [Slide] {
		
		let slide1:Slide = Bundle.main.loadNibNamed("Slide", owner: self, options: nil)?.first as! Slide
		slide1.imageView.image = UIImage(named: "flowIconCollege")
		slide1.descriptionTextView.text = "Earn free cash to save for college simply by shopping your favorite store and brands"
		
		let slide2:Slide = Bundle.main.loadNibNamed("Slide", owner: self, options: nil)?.first as! Slide
		slide2.imageView.image = UIImage(named: "flowIconCredit")
		slide2.descriptionTextView.text = "Link your every day credit card and get rewarded on top of existing discounts and rewards (including credit card rewards)"
		
		let slide3:Slide = Bundle.main.loadNibNamed("Slide", owner: self, options: nil)?.first as! Slide
		slide3.imageView.image = UIImage(named: "flowIconGraph")
		slide3.descriptionTextView.text = "Set up automatic transfer to your 529 and we'll take care of the rest. Don't worry, if you don't have a 529, you can still start saving with Belle"
		
		let slide4:Slide = Bundle.main.loadNibNamed("Slide", owner: self, options: nil)?.first as! Slide
		slide4.imageView.image = UIImage(named: "flowIconHeart")
		slide4.descriptionTextView.text = "100% of the rewards go to you. We never take a cut of your savings and it's always 100% free to use"
		
		
		return [slide1, slide2, slide3, slide4 ]
	}
}

extension UIPageControl {
	
	func customPageControl(dotFillColor:UIColor, dotBorderColor:UIColor, dotBorderWidth:CGFloat) {
		for (pageIndex, dotView) in self.subviews.enumerated() {
			if self.currentPage == pageIndex {
				dotView.backgroundColor = dotFillColor
				dotView.layer.cornerRadius = dotView.frame.size.height / 2
			}else{
				dotView.backgroundColor = .clear
				dotView.layer.cornerRadius = dotView.frame.size.height / 2
				dotView.layer.borderColor = dotBorderColor.cgColor
				dotView.layer.borderWidth = dotBorderWidth
			}
		}
	}
	
}

extension IntroFlowViewController: UIScrollViewDelegate {
	
	func scrollViewDidScroll(_ scrollView: UIScrollView) {
		let pageIndex = round(scrollView.contentOffset.x/view.frame.width)
		pageControl.currentPage = Int(pageIndex)
		pageControl.customPageControl(dotFillColor: UIColor.belleColor(), dotBorderColor: UIColor.belleColor(), dotBorderWidth: 1)
		
		let maximumHorizontalOffset: CGFloat = scrollView.contentSize.width - scrollView.frame.width
		let currentHorizontalOffset: CGFloat = scrollView.contentOffset.x
		
		// vertical
		let maximumVerticalOffset: CGFloat = scrollView.contentSize.height - scrollView.frame.height
		let currentVerticalOffset: CGFloat = scrollView.contentOffset.y
		
		let percentageHorizontalOffset: CGFloat = currentHorizontalOffset / maximumHorizontalOffset
		let percentageVerticalOffset: CGFloat = currentVerticalOffset / maximumVerticalOffset
		
		
		/*
		* below code changes the background color of view on paging the scrollview
		*/
		// self.scrollView(scrollView, didScrollToPercentageOffset: percentageHorizontalOffset)
		
		
		/*
		* below code scales the imageview on paging the scrollview
		*/
		/*
		let percentOffset: CGPoint = CGPoint(x: percentageHorizontalOffset, y: percentageVerticalOffset)
		
		if(percentOffset.x > 0 && percentOffset.x <= 0.25) {
			
			slides[0].imageView.transform = CGAffineTransform(scaleX: (0.25-percentOffset.x)/0.25, y: (0.25-percentOffset.x)/0.25)
			slides[1].imageView.transform = CGAffineTransform(scaleX: percentOffset.x/0.25, y: percentOffset.x/0.25)
			
		} else if(percentOffset.x > 0.25 && percentOffset.x <= 0.50) {
			slides[1].imageView.transform = CGAffineTransform(scaleX: (0.50-percentOffset.x)/0.25, y: (0.50-percentOffset.x)/0.25)
			slides[2].imageView.transform = CGAffineTransform(scaleX: percentOffset.x/0.50, y: percentOffset.x/0.50)
			
		} else if(percentOffset.x > 0.50 && percentOffset.x <= 0.75) {
			slides[2].imageView.transform = CGAffineTransform(scaleX: (0.75-percentOffset.x)/0.25, y: (0.75-percentOffset.x)/0.25)
			slides[3].imageView.transform = CGAffineTransform(scaleX: percentOffset.x/0.75, y: percentOffset.x/0.75)
			
		}
		*/
		/*
		else if(percentOffset.x > 0.75 && percentOffset.x <= 1) {
			slides[3].imageView.transform = CGAffineTransform(scaleX: (1-percentOffset.x)/0.25, y: (1-percentOffset.x)/0.25)
			slides[4].imageView.transform = CGAffineTransform(scaleX: percentOffset.x, y: percentOffset.x)
		}
		*/
	}

}
