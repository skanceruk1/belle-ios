//
//  BankAccount.swift
//  Belle
//
//  Created by Steve on 4/15/19.
//  Copyright © 2019 Belle. All rights reserved.
//

import Foundation

struct BankAccount {
	var id = ""
	var name = ""
	var official_name = ""
	var balance = ""
	var balanceNum:Float = 0.0
	var date = ""
	var subtype = ""
	var type = ""
	var mask = 0
}
