//
//  CreditCard.swift
//  Belle
//
//  Created by Steve on 4/12/19.
//  Copyright © 2019 Belle. All rights reserved.
//

import Foundation

struct CreditCard {
	
	
	var id = "7jp16ZvPooiBpB1ZxLQntlLGb48eWpfgGgyWo"
	var mask = 3333;
	var name = "Plaid Credit Card"
	var officialName = ""
	var subtype = "credit card"
	var type = "credit"
	var balance:Int?
	var bellePoints:Int?
	var available:Float?
	var current:Float?
	/*
	id = NdRQozVqJJfaAa7K41BJCjzGNn5L3xcW1Wpvp;
	mask = 0000;
	name = "Plaid Checking";
	subtype = checking;
	type = depository;
	
	id = P4bM83RrJJfwqwjJPKEmtknxmK5WZwH7475D6;
	mask = 1111;
	name = "Plaid Saving";
	subtype = savings;
	type = depository;
	
	id = jdbMjy3pvvfmKmZ5gaMnFrLkgNmz9bF131m5g;
	mask = 2222;
	name = "Plaid CD";
	subtype = cd;
	type = depository;
	*/
}
