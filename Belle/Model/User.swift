//
//  User.swift
//  Belle
//
//  Created by Steve on 4/11/19.
//  Copyright © 2019 Belle. All rights reserved.
//

import UIKit

class User: NSObject {
	
	var username:String?
	var email:String?
	var password:String?
	
	var name:String?
	var phoneNumber:Int?
	
	var numberOfChildren:Int?
//	var collegeStartDate:Int?
	var childrenStartYear:Int?
	var childrenGradYear:Int?
	
	var hasBankAccount = false
	var hasFiveTwentyNine = false
	var hasCreditCard = false
	var bankAccount:BankAccount?
//	var belleAccount:BelleAccount?
	var creditCard:CreditCard?
	
	var plaidPublicToken:String?
	var plaidAccessToken:String?
	
	var bankAccountID:String?
	var creditCardID:String?
	
	var bankAccountItemID:String?
	var creditCardItemD:String?
	
	var offersCommaSeparatedList:String?
	var testUser = false
	
	override init() {
		super.init()
	}
	
	init(username:String, email:String, password:String) {
		super.init()
		self.username = username
		self.email = email
		self.password = password
	}
	
	init (withJsonDict: [String: Any] ) {
		
		if let collegeStart = withJsonDict["college_start_date"] as? String {
			let split = collegeStart.split(separator: "-")
			if split.count > 0 {
				self.childrenStartYear = Int(split[0])
			}
		}
		if let email = withJsonDict["email"]  as? String {
			self.email = email
		}
		if let name = withJsonDict["name"]  as? String {
			self.name = name
		}
		if let username = withJsonDict["username"]  as? String {
			self.username = username
		}
		if let number_of_children = withJsonDict["number_of_children"]  as? Int {
			self.numberOfChildren = number_of_children
		}
		if let phone_number = withJsonDict["phone_number"]  as? Int {
			self.phoneNumber = phone_number
		}
		
//		if let plaid_public_token = withJsonDict["plaid_public_token"]  as? String {
//			BelleNetworkManager.shared.plaidPublicToken = plaid_public_token
//			self.plaidPublicToken = plaid_public_token
//		}
		
		if let plaid_access_token = withJsonDict["plaid_access_token"]  as? String {
			BelleNetworkManager.shared.plaidAccessToken = plaid_access_token
			self.plaidPublicToken = plaid_access_token
		}
		
		if let plaid_credit_card_ID = withJsonDict["plaid_credit_card_ID"]  as? String {
			self.creditCardID = plaid_credit_card_ID
		}
		if let plaid_bank_account_ID = withJsonDict["plaid_bank_account_ID"]  as? String {
			self.bankAccountID = plaid_bank_account_ID
		}
		if let plaid_credit_card_item_ID = withJsonDict["plaid_credit_card_item_ID"]  as? String {
			self.creditCardItemD = plaid_credit_card_item_ID 
		}
		if let plaid_bank_account_item_ID = withJsonDict["plaid_bank_account_item_ID"]  as? String {
			self.bankAccountItemID = plaid_bank_account_item_ID
		}
		if let test_user = withJsonDict["test_user"]  as? Bool {
			self.testUser = test_user
		}
		// default to false
		else {
			self.testUser = false
		}
		bankAccount = BankAccount()
		creditCard = CreditCard()
	}
	
	func toString() -> String {
		let userString = " username: \(String(describing: username)) \n email: \(String(describing: email)) \n password:\(String(describing: password))"
		let userStringOne = " name: \(String(describing: name)) \n phoneNumber: \(String(describing: phoneNumber)) \n numberOfChildren: \(String(describing: numberOfChildren))"
		let userStringTwo = "childrenStartYear: \(String(describing: childrenStartYear)) \n childrenGradYear: \(String(describing: childrenGradYear))"
		let userStringThree = "bankAccountId \(String(describing: bankAccountID)) creditCardId: \(String(describing: creditCardID))"
		let s = userString + userStringOne + userStringTwo + userStringThree
		return s
	}
	
	func printString() {
		print(self.toString())
	}
	
	func getEmail() -> String {
		if let e = email {
			return "\(e)"
		}
		else {
			return " "
		}
	}
	
	func getName() -> String {
		if let n = name {
			return "\(n)"
		}
		else {
			return " "
		}
	}
	
	func getPhoneNumberString() -> String {
		
		if let n = phoneNumber {
			
			if n > 1000000000 || n < 10000000000 {
				
				let areaCode = n/10000000 //six zeroes
				let lastFour = n%10000 // five zeroes
				let middleThree = (n%10000000)/10000  // six and five
				
				let s:String = "(\(areaCode)) \(middleThree) \(lastFour)"
				return s
			}
			
			return "\(n)"
			
		}
		return "(222) 222 2222"
	}
	
	func getCollegeStartString() -> String {
		
		if let start = childrenStartYear   {
			return "\(start)"
		}
		return " "
	}
	
	func getCollegeStartInt() -> Int { 
		if let start = childrenStartYear  {
			return  start
		}
		return 2019
	}
		
	func getNumberChildrenCollegeString() -> String {
		if let n = numberOfChildren {
			return "\(n)"
		}
		return " "
	}
	
	func offersArrayToString(array:[Int]) -> String {
		if (array.count == 0) {
			return ""
		}
		let a = array.sorted()
		var s = ""
		for i in 0..<(a.count - 1) {
			s += "\(i),"
		}
		s += "\(array[a.count - 1])"
		return s
	}
	
	func offerStringToArray(string:String) -> [Int]{
		if (string == "") {
			return [Int]()
		}
		var a = [Int]()
		let commaSeperated = string.components(separatedBy: ",")
		for sInt in commaSeperated {
			if let n = Int(sInt) {
				a.append(n)
			}
		}
		return a
	}
	
}
