//
//  BelleBackend.swift
//  Belle
//
//  Created by Steve on 3/25/19.
//  Copyright © 2019 Belle. All rights reserved.
//

import Foundation
import Alamofire

class BelleNetworkManager: NSObject {
	
	static let shared = BelleNetworkManager(baseURLString: Constants.BASE_URL_PRODUCTION)
	
	// MARK: -
	var belleBaseURL:String
	
	var plaidBaseURLString: String = Constants.PLAID_BASE_URL_DEVELOPMENT
	var plaidClientId: String =  Constants.PLAID_CLIENT_ID
	var plaidPublicKey: String = Constants.PLAID_PUBLIC_KEY 
	var plaidSecret: String  = Constants.PLAID_SECRET_DEVELOPMENT
	
	var plaidPublicToken: String
	var plaidPublicTokenBank: String
	
	var plaidAccessToken:String // = "access-sandbox-f95575b1-7448-4349-acff-ea4c6bb1fab5"
	// : String // = "access-sandbox-f95575b1-7448-4349-acff-ea4c6bb1fab5"
	var plaidAccessTokenBank: String
	
	var plaidCreditCardItemId: String
	var plaidBankAccountItemId: String
	
	var creditCardId = ""
	
	
	var plaidCreditCard: CreditCard
	
	var currentUser:User?
	
	var bankAccounts = [BankAccount]()
	
	var selectedOffers = Set<Int>()
	
	private init(baseURLString: String) {
		self.belleBaseURL = baseURLString
		self.plaidCreditCard = CreditCard()
		self.plaidPublicToken = ""
		self.plaidAccessToken = "" // "access-sandbox-f95575b1-7448-4349-acff-ea4c6bb1fab5"
		
		self.creditCardId = ""
		self.plaidCreditCardItemId = ""
		self.plaidBankAccountItemId = ""
		
		self.plaidAccessTokenBank = ""
		self.plaidPublicTokenBank = ""
		
		// For both
		plaidClientId = Constants.PLAID_CLIENT_ID
		plaidPublicKey = Constants.PLAID_PUBLIC_KEY
		
		// one or other for sandbox or develop
//		plaidBaseURLString = Constants.PLAID_BASE_URL_DEVELOPMENT
//		plaidSecret = Constants.PLAID_SECRET_DEVELOPMENT
		
		plaidBaseURLString = Constants.PLAID_BASE_URL_SANDBOX
		plaidSecret = Constants.PLAID_SECRET_SANDBOX
		
		/*
		plaidBaseURLString = Constants.PLAID_BASE_URL_SANDBOX
		plaidClientId = Constants.PLAID_CLIENT_ID_SANDBOX
		plaidPublicKey = Constants.PLAID_PUBLIC_KEY_SANDBOX
		plaidSecret = Constants.PLAID_SECRET_SANDBOX
		*/
	}
	
	func setPublicToken(publicToken:String) {
		self.plaidPublicToken = publicToken
	}
	
	func setPublicTokenBank(publicToken:String) {
		self.plaidPublicToken = publicToken
	}
	
	func getPublicToken() -> String {
		return plaidPublicToken
	}
	
	func setPlaidAccessToken(accessToken:String) {
		self.plaidPublicToken = accessToken
	}
	
	func setPlaidAccessTokenBank(accessToken:String) {
		self.plaidPublicTokenBank = accessToken
	}
	
	func getPlaidAccessToken() -> String {
		return plaidAccessToken
	}
	
	func setCreditCardId(id:String) {
		self.creditCardId = id
	}
	
	func setPlaidToSandbox() {
		self.plaidBaseURLString = Constants.PLAID_BASE_URL_SANDBOX
		
		self.plaidSecret = Constants.PLAID_SECRET_SANDBOX
		
//		self.plaidAccessToken = Constants.PLAID_ACCESS_TOKEN_SANDBOX
	}
	
	func setPlaidToDevelopment() {
		self.plaidBaseURLString = Constants.PLAID_BASE_URL_DEVELOPMENT
		
		self.plaidSecret = Constants.PLAID_SECRET_DEVELOPMENT
		
		//self.plaidAccessToken = Constants.PLAID_ACCESS_TOKEN_D
	}
	
	func signup(username:String, password1:String, password2:String, email:String,
				collegeStartYear:Int, name:String, numberOfChildren:Int, phoneNumber:Int, onSuccess:@escaping ()->(), onFailure:@escaping ((String))->()) {
//		let parameters: Parameters = ["username": "obama",
//									  "password1": "ihaveagood",
//									  "password2": "ihaveagood",
//									  "email": "obama@whitehouse.gov"]
		let parameters: Parameters = ["username": "\(username)", // should be email
									  "password1": "\(password1)",
									  "password2": "\(password2)",
									  "email": "\(email)",
									  "college_start_date": "\(collegeStartYear)-08-01",
									  "name": name,
									  "number_of_children": numberOfChildren,
									  "phone_number": phoneNumber,
									  "plaid_public_token": plaidPublicToken,
									  "plaid_access_token": plaidAccessToken,
									  "plaid_credit_card_ID": "plaid_credit_card_ID",
									  "plaid_bank_account_ID": "plaid_bank_account_ID",
									  "plaid_credit_card_item_ID": plaidCreditCardItemId,
									  "test_user": false]
		
									  //,
									  //"plaid_bank_account_item_ID": "plaid_bank_account_item_ID"]
		
		let headers:HTTPHeaders  = ["Content-Type": "application/json"]
		
		AF.request("\(belleBaseURL)/rest-auth/registration/",
			method: .post,
			parameters: parameters,
			encoding: JSONEncoding.default,
			headers: headers
			).responseJSON { response in
			
			if let error = response.error {
				print("JSON: \(error)")
				onFailure(error.localizedDescription)
				return
			}
			
			switch response.result {
			case .success:
				print("Validation Successful")
				
				
				
				print(response)
				print(response.result)
				print(response.result.self)
				 
				
				if let json = response.value as? [String: Any] {
					print("JSON: \(json)") // serialized json response
					guard let jsonDict = json as? [String: Any] else {
						onFailure("Something went wrong")
						return
					}
					if let username = jsonDict["username"] {
						let str = String(describing: username)
						let spl = str.components(separatedBy: "\n")
						// print(String(describing: username))
						if (spl.count > 1) {
							onFailure(String(describing: "Username:\(spl[1])"))
							return
						}
						onFailure(String(describing: username))
						return
					}
					if let email = jsonDict["email"] {
						let str = String(describing: email)
						let spl = str.components(separatedBy: "\n")
						// print(String(describing: username))
						if (spl.count > 1) {
							onFailure(String(describing: "Email:\(spl[1])"))
							return
						}
						print(String(describing: email))
						onFailure(String(describing: email))
						return
					}
					if let p1 = jsonDict["password1"] {
						let str = String(describing: p1)
						let spl = str.components(separatedBy: "\n")
						// print(String(describing: username))
						if (spl.count > 1) {
							onFailure(String(describing: "Password:\(spl[1])"))
							return
						}
						print(String(describing: p1))
						onFailure(String(describing: p1))
						return
					}
					if let p2 = jsonDict["password2"] {
						let str = String(describing: p2)
						let spl = str.components(separatedBy: "\n")
						// print(String(describing: username))
						if (spl.count > 1) {
							onFailure(String(describing: "Password:\(spl[1])"))
							return
						}
						print(String(describing: p2))
						onFailure(String(describing: p2))
						return
					}
					onSuccess()
				}
				else {
					onFailure("Something went wrong")
				}
			case .failure(let error):
				print(error)
				onFailure(error.localizedDescription)
			}
			
			
		}
	}
	
	func login(username:String, password:String,onSuccess:@escaping ()->(), onFailure:@escaping ((String))->()) {
//		let parameters: Parameters = ["username": "obama",
//									  "password": "ihaveagood",
//									  "email": "obama@whitehouse.gov"]
		let parameters: Parameters = ["username": "\(username)",
									  "password": "\(password)",
									  "email": "\(username)"]
		
		let headers:HTTPHeaders  = ["Content-Type": "application/json"]
		
		
		AF.request("\(belleBaseURL)/api/auth/login/",
			method: .post,
			parameters: parameters,
			encoding: JSONEncoding.default,
			headers: headers).responseJSON { response in
								
			if let error = response.error {
				print("JSON: \(error)")
				onFailure(error.localizedDescription)
				return
			}
			  
			 
				print("JSON: \(String(describing: response.value))") // serialized json response
				
				switch response.result {
				case .success:
					if let json = response.value as? [String: Any] {
						print("\(json)")
						
						if let key = json["key"] as? String {
							print("key: \(key)")
							print("Validation Successful")
							
							if let userJson = json["user"] as? [String: Any] {
								
								BelleNetworkManager.shared.currentUser = User(withJsonDict: userJson)
								BelleNetworkManager.shared.currentUser?.printString()
								
								/// account for test user before going along with normal flow
								// not used for now
								/*
								if (BelleNetworkManager.shared.currentUser!.testUser == true) {
									BelleNetworkManager.shared.setPlaidToSandbox()
								}
								else {
									BelleNetworkManager.shared.setPlaidToDevelopment()
								}
								*/
								onSuccess()
								return
							}
							
						}
						
					}
				case .failure(let error):
					print(error)
					onFailure(error.localizedDescription)
					return
				}
			
								
				if let json = response.value as? [String: Any] {
					print("\(json)")
					
					if let non_field_errors = json["non_field_errors"] as? [String] {
						print("errors: \(non_field_errors[0])")
						print("non_field_errors")
						var error = non_field_errors[0] //"Something went wrong"
						if non_field_errors.count > 0 {
							error = non_field_errors[0]
						}
						onFailure(error)
						return
					}
					// catch email address error
					else if let emailError = json["email"] as? [String] {
						let error = emailError[0]
						print(error)
						onFailure(error)
						return
					}
				}
								
				let error = "Something went wrong"
				print(error)
				onFailure(error)
		}
	}
	
	func logout() {
		
	}
	/*
	func plaidAuthentication(onSuccess:@escaping ()->(), onFailure:@escaping ((String))->()) {
		let parameters = ["client_id " : plaidClientId,
						  "secret" : plaidSecret,
						  "access_token" : plaidAccessToken]
						  // "access_token" : "oGWwBRxl8WfrXR1qD8dbiEKmX9plDXtRlZvVQ"]
		
		AF.request("\(plaidBaseURLString)/transactions/get",
			method: .post,
			parameters: parameters).responseJSON { response in
				
				print(String(describing: response))
				
				if let error = response.error {
					print("JSON: \(error)")
					onFailure(error.localizedDescription)
					return
				}
				
				// print(String(describing: response))
		}
	}
	*/
	
	func exchangeTokens(onSuccess:@escaping ()->(), onFailure:@escaping ((String))->()) {
		
		let parameters = ["client_id" : plaidClientId,
						  "secret" : plaidSecret,
						  "public_token" : plaidPublicToken]
		
		
		let headers:HTTPHeaders  = ["Content-Type": "application/json"]
		
		AF.request("\(plaidBaseURLString)/item/public_token/exchange",
			method: .post,
			parameters: parameters,
			encoding: JSONEncoding.default,
			headers: headers ).responseJSON { response in
				
				print(String(describing: response))
				
				if let error = response.error {
					print("JSON: \(error)")
					onFailure(error.localizedDescription)
					return
				}
				
				if let json = response.value as? [String: Any] {
					print("exchange tokens JSON: \(json)") // serialized json response
					
					
					if let item_id = json["item_id"] as? String {
						// self.plaidClientId = item_id
						self.plaidCreditCardItemId = item_id
					}
					else {
						self.plaidCreditCardItemId = "wut lol"
					}
					
					if let access_token = json["access_token"] as? String {
						self.plaidAccessToken = access_token
						onSuccess()
						return
					}
					
				}
				
				onFailure("Something went wrong with connecting to Plaid")
				// print(String(describing: response))
		}
	}
	
	func exchangeTokensBank(onSuccess:@escaping ()->(), onFailure:@escaping ((String))->()) {
		
		let parameters = ["client_id" : plaidClientId,
						  "secret" : plaidSecret,
						  "public_token" : plaidPublicTokenBank]
		let headers:HTTPHeaders  = ["Content-Type": "application/json"]
		
		AF.request("\(plaidBaseURLString)/item/public_token/exchange",
			method: .post,
			parameters: parameters,
			encoding: JSONEncoding.default,
			headers: headers ).responseJSON { response in
				
				print(String(describing: response))
				
				if let error = response.error {
					print("JSON: \(error)")
					onFailure(error.localizedDescription)
					return
				}
				
				if let json = response.value as? [String: Any] {
					print("exchange tokens JSON: \(json)") // serialized json response
					
					
					if let item_id = json["item_id"] as? String {
						//self.plaidClientId = item_id
						self.plaidBankAccountItemId = item_id
					}
					else {
						self.plaidCreditCardItemId = "wut lol"
					}
					
					if let access_token = json["access_token"] as? String {
						self.plaidAccessTokenBank = access_token
						onSuccess()
						return
					}
					
				}
				
				onFailure("Something went wrong with connecting to Plaid")
				// print(String(describing: response))
		}
	}
	
	/*
	func getIncome( onSuccess:@escaping ()->(), onFailure:@escaping ((String))->()) {
		
		let parameters = ["client_id" : plaidClientId,
						  "secret" : plaidSecret,
						  "access_token" : plaidAccessToken]
		let headers:HTTPHeaders = ["Content-Type": "application/json"]
		
		AF.request("\(plaidBaseURLString)/income/get",
			method: .post,
			parameters: parameters,
			encoding: JSONEncoding.default,
			headers: headers ).responseJSON { response in
				
				if let error = response.error {
					print("JSON: \(error)")
					onFailure(error.localizedDescription)
					return
				}
				
				print(String(describing: response))
				
		}
	}
	*/
	
	func getBalance(onSuccess:@escaping ()->(), onFailure:@escaping ((String))->()) {
		
		let parameters = ["client_id" : plaidClientId,
						  "secret" : plaidSecret,
						  "access_token" : plaidAccessToken]
		let headers:HTTPHeaders = ["Content-Type": "application/json"]
		
		AF.request("\(plaidBaseURLString)/accounts/balance/get",
			method: .post,
			parameters: parameters,
			encoding: JSONEncoding.default,
			headers: headers ).responseJSON { response in
				
				// print(String(describing: response))
				
				if let error = response.error {
					print("JSON: \(error)")
					onFailure(error.localizedDescription)
					return
				}
				
				print(String(describing: response))
				
				if let json = response.value as? [String: Any] {
					print("JSON: \(json)") // serialized json response
					
					if let accounts = json["accounts"] as? [[String : Any]] {
						// print(String(describing: accounts))
						for account in accounts {
							print(String(describing: "   \(account)"))
							if let type = account["type"] as? String {
								
								
								if  type == "depository" {
									
									if let subtype = account["subtype"] as? String { //
										
										if subtype == "savings" { // this will be the bank account
										
											if let balances = account["balances"] as? [String: Any] {
												
												if let currentBalance = balances["current"] as? Int {
													
													
													BelleNetworkManager.shared.currentUser?.creditCard?.balance = currentBalance
												}
												
											}
											
										}
										if subtype == "cd" { // this will be the belle account for now
										
											if let balances = account["balances"] as? [String: Any] {
												
												if let currentBalance = balances["current"] as? Float {
													BelleNetworkManager.shared.currentUser?.bankAccount?.balanceNum = currentBalance
												}
												
											}
//											BelleNetworkManager.shared.currentUser?.belleAccount?.balance = "etetete"
										}
										
									}
								}
								if  type == "credit" {
									if let id = account["id"] as? String {
										BelleNetworkManager.shared.setCreditCardId(id: String(describing: id))
										BelleNetworkManager.shared.currentUser?.creditCard?.id = id
									}
								}
								if  type == "credit" {
									if let id = account["id"]  as? String {
										BelleNetworkManager.shared.setCreditCardId(id: id)
									}
								}
							}
						}
					}
					
				}
				
				onSuccess()
		}
	}
	
	func getBalanceWithBankAccountId(bankAccountId:String, onSuccess:@escaping ()->(), onFailure:@escaping ((String))->()) {
		
		let parameters = ["client_id" : plaidClientId,
						  "secret" : plaidSecret,
						  "access_token" : plaidAccessToken]
		let headers:HTTPHeaders = ["Content-Type": "application/json"]
		
		AF.request("\(plaidBaseURLString)/accounts/balance/get",
			method: .post,
			parameters: parameters,
			encoding: JSONEncoding.default,
			headers: headers ).responseJSON { response in
				
				// print(String(describing: response))
				
				if let error = response.error {
					print("JSON: \(error)")
					onFailure(error.localizedDescription)
					return
				}
				
				print(String(describing: response))
				
				if let json = response.value as? [String: Any] {
					print("JSON: \(json)") // serialized json response
					
					if let accounts = json["accounts"] as? [[String : Any]] {
						// print(String(describing: accounts))
						for account in accounts {
							print(String(describing: "   \(account)"))
							if let type = account["type"] as? String {
								
								
								if  type == "depository" {
									
									if let id = account["account_id"] as? String { //
										
										if id == bankAccountId {
											
											if let balances = account["balances"] as? [String: Any] {
												
												let b = balances
												print( balances)
												print("current  \(String(describing: balances["current"]))")
												print("available  \(String(describing: balances["available"]))")
												
												
												if let currentBalance = balances["available"] as? Float {
													BelleNetworkManager.shared.currentUser?.bankAccount?.balanceNum = Float(currentBalance)
													
													let balanceString = String(currentBalance)
													BelleNetworkManager.shared.currentUser?.bankAccount?.balance = balanceString
													
													break
												}
												else if let currentBalance = balances["available"] as? Double {
													BelleNetworkManager.shared.currentUser?.bankAccount?.balanceNum = Float(currentBalance)
													
													let balanceString = String(currentBalance)
													BelleNetworkManager.shared.currentUser?.bankAccount?.balance = balanceString
													break
												}
												else if let currentBalance = balances["available"] as? Int {
													
													
													BelleNetworkManager.shared.currentUser?.bankAccount?.balanceNum = Float(currentBalance)
													
													let balanceString = String(currentBalance)
													BelleNetworkManager.shared.currentUser?.bankAccount?.balance = balanceString
													break
												}
												else if let currentBalance = balances["current"] as? String {
													
															/*
														BelleNetworkManager.shared.currentUser?.creditCard?.balance = currentBalance
															*/
													
													BelleNetworkManager.shared.currentUser?.bankAccount?.balance = currentBalance
													
													let balanceString = String(currentBalance)
													
													if let balanceFloat = Float(currentBalance) {
													
														BelleNetworkManager.shared.currentUser?.bankAccount?.balanceNum = balanceFloat
													}
													break
												}
												else if let currentBalance = balances["current"] as? Float {
													
													/*
													BelleNetworkManager.shared.currentUser?.creditCard?.balance = currentBalance
													*/
													
													BelleNetworkManager.shared.currentUser?.bankAccount?.balanceNum = currentBalance
													
													 let balanceString = String(currentBalance)
													
													// if let balanceString = String(currentBalance) {
													BelleNetworkManager.shared.currentUser?.bankAccount?.balance = balanceString
													// }
													break
												}
												else if let currentBalance = balances["current"] as? Double {
													
													/*
													BelleNetworkManager.shared.currentUser?.creditCard?.balance = currentBalance
													*/
													
													BelleNetworkManager.shared.currentUser?.bankAccount?.balanceNum = Float(currentBalance)
													
													let balanceString = String(currentBalance)
													
													// if let balanceString = String(currentBalance) {
													BelleNetworkManager.shared.currentUser?.bankAccount?.balance = balanceString
													// }
													break
												}
												else if let currentBalance = balances["current"] as? Int {
													
													/*
													BelleNetworkManager.shared.currentUser?.creditCard?.balance = currentBalance
													*/
													
													BelleNetworkManager.shared.currentUser?.bankAccount?.balanceNum = Float(currentBalance)
													
													let balanceString = String(currentBalance)
													
													// if let balanceString = String(currentBalance) {
													BelleNetworkManager.shared.currentUser?.bankAccount?.balance = balanceString
													// }
													break
												}
												else if let currentBalance = balances["available"] as? String {
													
													/*
													BelleNetworkManager.shared.currentUser?.creditCard?.balance = currentBalance
													*/
													
													BelleNetworkManager.shared.currentUser?.bankAccount?.balance = currentBalance
													
													let balanceString = String(currentBalance)
													
													if let balanceFloat = Float(currentBalance) {
													BelleNetworkManager.shared.currentUser?.bankAccount?.balanceNum = balanceFloat
													}
													break
												}
												else {
													BelleNetworkManager.shared.currentUser?.bankAccount?.balanceNum = 8
													BelleNetworkManager.shared.currentUser?.bankAccount?.balance = "8"
												}
											}
										}
									}
									
								}
								
							}
						}
					}
					
				}
				onSuccess()
		}
	}
	
	func getBankInfo(onSuccess:@escaping ()->(), onFailure:@escaping ((String))->()) {
		
		let parameters = ["client_id" : plaidClientId,
			"secret" : plaidSecret,
			"access_token" : plaidAccessTokenBank]
		let headers:HTTPHeaders = ["Content-Type": "application/json"]
		
		AF.request("\(plaidBaseURLString)/accounts/get",
			method: .post,
			parameters: parameters,
			encoding: JSONEncoding.default,
			headers: headers ).responseJSON { response in
				
				// print(String(describing: response))
				
				if let error = response.error {
					print("JSON: \(error)")
					onFailure(error.localizedDescription)
					return
				}
				
				print(String(describing: response))
				
				if let json = response.value as? [String: Any] {
					print("JSON: \(json)") // serialized json response
					
					if let accounts = json["accounts"] as? [[String : Any]] {
						// print(String(describing: accounts))
						for account in accounts {
							print(String(describing: "   \(account)"))
							if let type = account["type"] as? String {
								
								/*
								if  type == "depository" {
								
								if let subtype = account["subtype"] as? String { //
								
								if subtype == "savings" { // this will be the bank account
								
								if let balances = account["balances"] as? [String: Any] {
								
								if let currentBalance = balances["current"] as? Int {
								
								
								BelleNetworkManager.shared.currentUser?.creditCard?.balance = currentBalance
								}
								
								}
								
								}
								if subtype == "cd" { // this will be the belle account for now
								
								if let balances = account["balances"] as? [String: Any] {
								
								if let currentBalance = balances["current"] as? Int {
								BelleNetworkManager.shared.currentUser?.bankAccount?.balance = currentBalance
								}
								
								}
								//											BelleNetworkManager.shared.currentUser?.belleAccount?.balance = "etetete"
								}
								
								}
								}
								*/
								
							}
						}
					}
					
				}
				
				onSuccess()
		}
	}
	
	/*

	this is the one that is actually used

	*/
	
	func getBankAccountsInfo(onSuccess:@escaping ()->(), onFailure:@escaping ((String))->()) {
		
		let parameters = ["client_id" : plaidClientId,
						  "secret" : plaidSecret,
						  "access_token" : plaidAccessToken]
		
		
		let headers:HTTPHeaders = ["Content-Type": "application/json"]
		
		
		
		AF.request("\(plaidBaseURLString)/accounts/balance/get",
			method: .post,
			parameters: parameters,
			encoding: JSONEncoding.default,
			headers: headers ).responseJSON { response in
				
				// print(String(describing: response))
				
				if let error = response.error {
					print("JSON: \(error)")
					onFailure(error.localizedDescription)
					return
				}
				
				print(String(describing: response))
				
				if let json = response.value as? [String: Any] {
					print("JSON: \(json)") // serialized json response
					
					if let accounts = json["accounts"] as? [[String : Any]] {
						// print(String(describing: accounts))
						for account in accounts {
							print(String(describing: "   \(account)"))
							if let type = account["type"] as? String {
								
								/*
								
								plus temporary bank stuff
								
								*/
								
								
								
								if  type == "depository" {
									
									var oneBankAccount = BankAccount()
									oneBankAccount.type = type
									
									if let id = account["account_id"] as? String { //
										oneBankAccount.id = id
									}
									
									if let mask = account["mask"] as? Int { //
										oneBankAccount.mask = mask
									}
									
									if let name = account["name"] as? String { //
										oneBankAccount.name = name
									}
									
									if let official_name = account["official_name"] as? String { //
										oneBankAccount.official_name = official_name
									}
									
									if let subtype = account["subtype"] as? String { //
										oneBankAccount.subtype = subtype
									}
									
									self.bankAccounts.append(oneBankAccount)
								}
							}
						}
					}
					
				}
				
				onSuccess()
		}
		/*
		let parameters = ["client_id" : plaidClientId,
						  "secret" : plaidSecret,
						  "access_token" : plaidAccessToken]
		let headers:HTTPHeaders = ["Content-Type": "application/json"]
		
		AF.request("\(plaidBaseURLString)/accounts/balance/get",
			method: .post,
			parameters: parameters,
			encoding: JSONEncoding.default,
			headers: headers ).responseJSON { response in
				
				// print(String(describing: response))
				
				if let error = response.error {
					print("JSON: \(error)")
					onFailure(error.localizedDescription)
					return
				}
				
				print(String(describing: response))
				
				if let json = response.value as? [String: Any] {
					print("JSON: \(json)") // serialized json response
					
					if let accounts = json["accounts"] as? [[String : Any]] {
						// print(String(describing: accounts))
						for account in accounts {
							print(String(describing: "   \(account)"))
							if let type = account["type"] as? String {
								
								if  type == "depository" {
									
									var oneBankAccount = BankAccount()
									oneBankAccount.type = type
									
									if let id = account["id"] as? String { //
										oneBankAccount.id = id
									}
									
									if let mask = account["mask"] as? Int { //
										oneBankAccount.mask = mask
									}
									
									if let name = account["name"] as? String { //
										oneBankAccount.name = name
									}
									
									if let subtype = account["subtype"] as? String { //
										oneBankAccount.subtype = subtype
									}
									
									self.bankAccounts.append(oneBankAccount)
								}
							}
						}
						let cc = BelleNetworkManager.shared.currentUser?.creditCard
					}
				}
		}
		
	}
	
	onSuccess()
	*/
	}
	
	func getCreditInfo(onSuccess:@escaping ()->(), onFailure:@escaping ((String))->()) {
		
		let parameters = ["client_id" : plaidClientId,
						  "secret" : plaidSecret,
						  "access_token" : plaidAccessToken]
		let headers:HTTPHeaders = ["Content-Type": "application/json"]
		
		AF.request("\(plaidBaseURLString)/accounts/balance/get",
			method: .post,
			parameters: parameters,
			encoding: JSONEncoding.default,
			headers: headers ).responseJSON { response in
				
				// print(String(describing: response))
				
				if let error = response.error {
					print("JSON: \(error)")
					onFailure(error.localizedDescription)
					return
				}
				
				print(String(describing: response))
				
				if let json = response.value as? [String: Any] {
					print("JSON: \(json)") // serialized json response
					
					if let accounts = json["accounts"] as? [[String : Any]] {
						// print(String(describing: accounts))
						for account in accounts {
							print(String(describing: "   \(account)"))
							if let type = account["type"] as? String {
								
								/*
								
								plus temporary bank stuff
								
								*/
								
								
								
								if  type == "credit" {
									
									print("credit card accout: ")
									print(account)
									
									if let id = account["account_id"] as? String {
										BelleNetworkManager.shared.setCreditCardId(id: String(describing: id))
										BelleNetworkManager.shared.currentUser?.creditCard?.id = id
									}
									if let name = account["name"] as? String {
										BelleNetworkManager.shared.currentUser?.creditCard?.name = name
									}
									if let official_name = account["official_name"] as? String { 	BelleNetworkManager.shared.currentUser?.creditCard?.officialName = official_name
									}
									
									if let balances = account["balances"] as? [String:Any] {
										if let available = balances["available"] as? Float { 	BelleNetworkManager.shared.currentUser?.creditCard?.available = available
										}
										if let current = balances["current"] as? Float { 	BelleNetworkManager.shared.currentUser?.creditCard?.current = current
										}
									}
									
								}
								
							}
						}
					}
					
				}
				
				onSuccess()
		}
	}
	
	func getCreditTransactions( onSuccess:@escaping ([Transaction])->(), onFailure:@escaping (String)->()) {
		
		let parameters:[String: Any] = ["client_id" : plaidClientId,
										"secret" : plaidSecret,
										"access_token" : plaidAccessToken,
										"start_date": "2019-03-21",
										"end_date": "2019-04-21"]//,
		//"account_id": "GW5p4Mzkk4fKRQVKK8pASrB7WvKoMri1Bxk8B"]
		//"account_ids": ["GW5p4Mzkk4fKRQVKK8pASrB7WvKoMri1Bxk8B"] ] //,
		//"count": 10]
		
		let headers:HTTPHeaders = ["Content-Type": "application/json"]
		
		AF.request("\(plaidBaseURLString)/transactions/get",
			method: .post,
			parameters: parameters,
			encoding: JSONEncoding.default,
			headers: headers ).responseJSON { response in
				
				print(String(describing: response))
				
				if let error = response.error {
					print("JSON: \(error)")
					onFailure(error.localizedDescription)
					return
				}
				
				if let json = response.value as? [String:Any] {
					
					
//					if let accounts = json["accounts"] as? [String:Any] {
//
//					}
//					if let item = json["item"] as? [String:Any] {
//
//					}
					
					var transactionArray = [Transaction]()
					if let transactions = json["transactions"] as? [[String:Any]] {
//						print(transactions)
						for trans in transactions {
							print("\n one")
							print("\(trans)")
							var transaction = Transaction()
							
							if let accountId = trans["account_id"] as? String {
								let cid = BelleNetworkManager.shared.creditCardId
								if accountId == BelleNetworkManager.shared.creditCardId {
									print("     hit     ")
								}
								let cidtwo = BelleNetworkManager.shared.currentUser?.creditCardID
								if accountId == BelleNetworkManager.shared.currentUser?.creditCardID {
									print("     hit     ")
								}
								
								if accountId == cid || accountId == cidtwo {
									
									if let name = trans["name"] as? String {
										transaction.name = name
									}
									if let date = trans["date"] as? String {
										transaction.date = date
									}
									
									if let amount = trans["amount"] as? Float {
										transaction.amount = "$\(amount*0.03)"
										transaction.amountNum = amount*0.03
										/// only add amounts that are positive
										if amount > 0 {
											transactionArray.append(transaction)
										}
									}
									
								}
							}
							
							
							 
						}
						
						onSuccess(transactionArray)
//						NotificationCenter.default.post(name: .CameraManagerDidStartSession, object: nil)
						NotificationCenter.default.post(name: NSNotification.Name("transactionsCompletedSuccesfully"), object: nil)
					}
					
				}
				
				else {
					onFailure("Something went wrong")
				}
				
		}
	}
	
}
