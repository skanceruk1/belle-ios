//
//  LastViewController.swift
//  Belle
//
//  Created by Steve on 3/9/19.
//  Copyright © 2019 Belle. All rights reserved.
//

import UIKit

class AccountViewController: UIViewController {

	@IBOutlet weak var signOutButton: UIButton!
	
	@IBOutlet weak var scrollView: UIScrollView!
	
	
	@IBOutlet weak var emailTextField: UITextField!
	
	@IBOutlet weak var phoneTextField: UITextField!
	
	@IBOutlet weak var changePasswordButton: UIButton!
	
	@IBOutlet weak var nameTextField: UITextField!
	
	@IBOutlet weak var collegeStartTextField: UITextField!
	
	@IBOutlet weak var childrenAttendingCollegeTextField: UITextField!
	
	override func viewDidLoad() {
        super.viewDidLoad()

		signOutButton.layer.cornerRadius = 10
		signOutButton.backgroundColor = UIColor.belleColor()
		changePasswordButton.setTitleColor(UIColor.belleColor(), for: .normal)  
		
		registerForKeyboardNotifications()
		
        // Do any additional setup after loading the view. 
		let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
		view.addGestureRecognizer(tap)
		
		populateTextFieldsWithAccountInfo()
		
    }
	
	func populateTextFieldsWithAccountInfo() {
		self.emailTextField.text = BelleNetworkManager.shared.currentUser?.getEmail()
		
		self.phoneTextField.text =  BelleNetworkManager.shared.currentUser?.getPhoneNumberString()
		
		self.nameTextField.text =  BelleNetworkManager.shared.currentUser?.getName()
		
		self.collegeStartTextField.text = BelleNetworkManager.shared.currentUser?.getCollegeStartString()
		
		self.childrenAttendingCollegeTextField.text = BelleNetworkManager.shared.currentUser?.getNumberChildrenCollegeString()
		
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		registerForKeyboardNotifications()
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		unregisterForKeyboardNotifications()
	}
	
	func registerForKeyboardNotifications() {
		NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardDidShowNotification, object: nil)
		NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: UIResponder.keyboardDidHideNotification, object: nil)
	}
	
	// Don't forget to unregister when done
	func unregisterForKeyboardNotifications()  {
		NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardDidShowNotification, object: nil)
		NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardDidHideNotification, object: nil)
	}
	
	@IBAction func backPressed(_ sender: Any) {
		self.navigationController?.popViewController(animated: true)
	}
	
	@IBAction func signOutPressed(_ sender: Any) {
		performSegue(withIdentifier: "unwindSegueToBeginning", sender: self)
	}
	
	@IBAction func changePasswordButtonPressed(_ sender: Any) {
		alert(withTitle: "Comming soon", description: "Feature comming soon!")
	}
	
	@objc func dismissKeyboard() {
		//Causes the view (or one of its embedded text fields) to resign the first responder status.
		view.endEditing(true)
	}
	
	/*
	@objc func keyboardWillShow(_ notification:NSNotification){
		var userInfo = notification.userInfo!
		var keyboardFrame:CGRect = (userInfo[UIResponder.keyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
		keyboardFrame = self.view.convert(keyboardFrame, from: nil)
		
		var contentInset:UIEdgeInsets = self.scrollView.contentInset
		contentInset.bottom = keyboardFrame.size.height
		scrollView.contentInset = contentInset
	}
	
	@objc func keyboardWillHide(_ notification:NSNotification){
		
		let contentInset:UIEdgeInsets = UIEdgeInsets.zero
		scrollView.contentInset = contentInset
	}
	*/
	
	@objc func keyboardWillShow(_ notification:NSNotification) {
		guard let keyboardFrameValue = notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue else {
			return
		}
		let keyboardFrame = view.convert(keyboardFrameValue.cgRectValue, from: nil)
		scrollView.contentOffset = CGPoint(x:0, y:keyboardFrame.size.height)
	}
	
	@objc func keyboardWillHide(_ notification:NSNotification) {
		scrollView.contentOffset = .zero
	}
	
	/*
	@objc func onKeyboardAppear(_ notification: NSNotification) {
		let info = notification.userInfo!
		let rect: CGRect = info[UIResponder.keyboardFrameBeginUserInfoKey] as! CGRect
		let kbSize = rect.size
		
		let insets = UIEdgeInsets(top: 0, left: 0, bottom: kbSize.height, right: 0)
		scrollView.contentInset = insets
		scrollView.scrollIndicatorInsets = insets
		
		// If active text field is hidden by keyboard, scroll it so it's visible
		// Your application might not need or want this behavior.
		var aRect = self.view.frame;
		aRect.size.height -= kbSize.height;
		
		let activeField: UITextField? = [emailTextField, phoneTextField, nameTextField, collegeStartTextField, childrenAttendingCollegeTextField].first { $0.isFirstResponder }
		
		if let activeField = activeField {
			if aRect.contains(activeField.frame.origin) {
				let scrollPoint = CGPoint(x: 0, y: activeField.frame.origin.y-kbSize.height)
				scrollView.setContentOffset(scrollPoint, animated: true)
			}
		}
	}
	
	@objc func onKeyboardDisappear(_ notification: NSNotification) {
		scrollView.contentInset = UIEdgeInsets.zero
		scrollView.scrollIndicatorInsets = UIEdgeInsets.zero
	}
	*/
}

extension AccountViewController: UITextFieldDelegate {
	
	
	
}
