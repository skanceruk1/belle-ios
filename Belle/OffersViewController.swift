//
//  OffersViewController.swift
//  Belle
//
//  Created by Steve on 3/14/19.
//  Copyright © 2019 Belle. All rights reserved.
//

import UIKit

class OffersViewController: UIViewController {

	@IBOutlet weak var offersSegmentedControl: UISegmentedControl!
	
	@IBOutlet weak var offersTableView: UITableView!
	
	@IBOutlet weak var offersLabel: UILabel!
	
	@IBOutlet weak var backButton: UIButton!
	
	var toggleEnabled = false // it was an option to take this off
	var selectedOffers = Set<Int>()
	
	override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
		let offerNib = UINib(nibName: "OffersTableViewCell", bundle: nil)
		offersTableView.register(offerNib, forCellReuseIdentifier: "offerCell")
		let titleNib = UINib(nibName: "TitleTableViewCell", bundle: nil)
		offersTableView.register(titleNib, forCellReuseIdentifier: "titleCell")
		
		offersTableView.delegate = self
		offersTableView.dataSource = self
//		offersTableView.rowHeight = UITableView.automaticDimension
		offersTableView.estimatedRowHeight = 110
		 offersTableView.allowsSelection = true
		
		if (toggleEnabled) {
			offersLabel.isHidden = true
			offersLabel.isEnabled = false
			offersLabel.isUserInteractionEnabled = false
		}
		else {
			backButton.isHidden = true
			backButton.isEnabled = false
			backButton.isUserInteractionEnabled = false 
		}
		
//		selectedOffers.insert(0)
//		selectedOffers.insert(3)
//		selectedOffers.insert(5)
			//BelleNetworkManager.shared.selectedOffers
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
//		BelleNetworkManager.shared.selectedOffers = [0, 3, 5]
//		if (BelleNetworkManager.shared.selectedOffers.count > 0) {
//			for i in BelleNetworkManager.shared.selectedOffers {
//
////				offersTableView.selectRow(at: IndexPath(row: i, section: 0), animated: true, scrollPosition: .none)
////				offersTableView.reloadData()
//				//let indexPath = IndexPath(row: i, section: 0)
//				//offersTableView(offersTableView, didSelectRowAtIndexPath: indexPath);
//			}
//		}
		selectedOffers = BelleNetworkManager.shared.selectedOffers
		offersTableView.reloadData()
	}

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

	@IBAction func offersButtonPressed(_ sender: Any) {
		if (toggleEnabled) {
			self.navigationController?.popViewController(animated: true)
		}
	}
}

extension OffersViewController: UITableViewDataSource {
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return 5
		// return 7
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		
		let offerCell = offersTableView.dequeueReusableCell(withIdentifier: "offerCell", for: indexPath) as! OffersTableViewCell
		offerCell.tintColor = UIColor.belleColor()
		
		switch indexPath.row {
		case 0:
			offerCell.offerLogoImage.image = UIImage(named: "offerLogoDunkinDonuts")
			offerCell.offerCompanyLabel?.text = "Dunkin Donuts: "
			offerCell.offerAmountLabel.text = "5%"
		case 1:
			offerCell.offerLogoImage.image = UIImage(named: "offerLogoWholeFoods")
			offerCell.offerCompanyLabel?.text = "Whole Foods: "
			offerCell.offerAmountLabel.text = "5%"
		case 2:
			offerCell.offerLogoImage.image = UIImage(named: "offerLogoSpotify")
			offerCell.offerCompanyLabel?.text = "Spotify: "
			offerCell.offerAmountLabel.text = "5%"
		case 3:
			offerCell.offerLogoImage.image = UIImage(named: "offerLogoLyft")
			offerCell.offerCompanyLabel?.text = "Lyft: "
			offerCell.offerAmountLabel.text = "3%"
		case 4:
			offerCell.offerLogoImage.image = UIImage(named: "offerLogoVerizon")
			offerCell.offerCompanyLabel?.text = "Verizon: "
			offerCell.offerAmountLabel.text = "3%"
		default:
			offerCell.offerCompanyLabel?.text = "offer"
			offerCell.offerAmountLabel.text = "5%"
		}
		
		/*
			title cells
		*/
		/*
		if indexPath.row == 0 {
			let offerCell = offersTableView.dequeueReusableCell(withIdentifier: "titleCell", for: indexPath) as! TitleTableViewCell
			offerCell.titleLabel.text = "My offers (1):"
			return offerCell
		}
		
		if indexPath.row == 2 {
			let offerCell = offersTableView.dequeueReusableCell(withIdentifier: "titleCell", for: indexPath) as! TitleTableViewCell
			offerCell.titleLabel.text = "New offers (4):"
			return offerCell
		}
		*/
		/*
			offer cells
		*/
		/*
		let offerCell = offersTableView.dequeueReusableCell(withIdentifier: "offerCell", for: indexPath) as! OffersTableViewCell
		
		switch indexPath.row {
		case 1:
			offerCell.offerLogoImage.image = UIImage(named: "offerLogoDunkinDonuts")
			offerCell.offerCompanyLabel?.text = "Dunkin Donuts: "
			offerCell.offerAmountLabel.text = "5%"
		case 3:
			offerCell.offerLogoImage.image = UIImage(named: "offerLogoWholeFoods")
			offerCell.offerCompanyLabel?.text = "Whole Foods: "
			offerCell.offerAmountLabel.text = "5%"
		case 4:
			offerCell.offerLogoImage.image = UIImage(named: "offerLogoSpotify")
			offerCell.offerCompanyLabel?.text = "Spotify: "
			offerCell.offerAmountLabel.text = "5%"
		case 5:
			offerCell.offerLogoImage.image = UIImage(named: "offerLogoLyft")
			offerCell.offerCompanyLabel?.text = "Lyft: "
			offerCell.offerAmountLabel.text = "3%"
		case 6:
			offerCell.offerLogoImage.image = UIImage(named: "offerLogoVerizon")
			offerCell.offerCompanyLabel?.text = "Verizon: "
			offerCell.offerAmountLabel.text = "3%"
		default:
			offerCell.offerCompanyLabel?.text = "offer"
			offerCell.offerAmountLabel.text = "5%" 
		}
		*/
		if (selectedOffers.contains(indexPath.row)) {
			// offersTableView.selectRow(at: indexPath, animated: false, scrollPosition: .none)
			offerCell.accessoryType = .checkmark
		}
		else {
			offerCell.accessoryType = .none
		}
		return offerCell
	}
	
}


extension OffersViewController: UITableViewDelegate {
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		print("lol row  \(indexPath.row)")
		//if (toggleEnabled) {
			let cell = tableView.cellForRow(at: indexPath)
			if (cell?.accessoryType == .checkmark) {
				cell?.accessoryType = .none
				cell?.tintColor = UIColor.belleColor()
				BelleNetworkManager.shared.selectedOffers.remove(indexPath.row)
			}
			else {
				cell?.accessoryType = .checkmark
				BelleNetworkManager.shared.selectedOffers.insert(indexPath.row)
			} 
			tableView.deselectRow(at: indexPath, animated: false)
		//}
	}
	
	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		/*
		if indexPath.row == 0 || indexPath.row == 2 {
			return 60
		}
		*/
		return 110
	}
	
}
