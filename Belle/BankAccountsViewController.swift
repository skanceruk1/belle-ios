//
//  BankAccountsViewController.swift
//  Belle
//
//  Created by Steve on 4/30/19.
//  Copyright © 2019 Belle. All rights reserved.
//

import UIKit
import SVProgressHUD

class BankAccountsViewController: UIViewController {

	@IBOutlet weak var backButton: UIButton!
	
	@IBOutlet weak var titleLabel: UILabel!
	
	@IBOutlet weak var tableView: UITableView!
	
	var bankAccounts = [BankAccount]()
	
	var onBankAccountSelected: ((String)->())?
	
	override func viewDidLoad() {
        super.viewDidLoad()
		
        // Do any additional setup after loading the view.
		let nibCreditRewardsButForBankAccounts = UINib(nibName: "CreditCardRewardTableViewCell", bundle: nil)
		self.bankAccounts = BelleNetworkManager.shared.bankAccounts
		
		tableView.register(nibCreditRewardsButForBankAccounts, forCellReuseIdentifier: "creditRewards")
		tableView.dataSource = self
		tableView.delegate = self
		
    }
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		self.bankAccounts = BelleNetworkManager.shared.bankAccounts
		// tableView.reloadData()
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
	}
 
	@IBAction func backButtonPressed(_ sender: Any) {
		self.navigationController?.popViewController(animated: true)
	}
	
}


extension BankAccountsViewController: UITableViewDelegate {
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		
		let bankAccount = bankAccounts[indexPath.row]
		
		let bankAccountId = bankAccount.id
		
		BelleNetworkManager.shared.currentUser?.bankAccountID = bankAccountId
		// update user bank id
		
		SVProgressHUD.show()
		BelleNetworkManager.shared.getBalanceWithBankAccountId(bankAccountId: bankAccountId,
															   onSuccess: {
															
																BelleNetworkManager.shared.currentUser?.bankAccountItemID = bankAccountId
																
																
																
																self.navigationController?.popViewController(animated: true)
																self.onBankAccountSelected?(bankAccountId)
																
																// Constants.BANK_ACCOUNT_CHOSEN
																NotificationCenter.default.post(name: NSNotification.Name(rawValue: Constants.BANK_ACCOUNT_CHOSEN), object: nil)
																
		},
															   
															   onFailure: { errorDesc in
//																self.navigationController?.popViewController(animated: true)
//																onBankAccountSelected?(bankAccountId)
																print("on bank account selected \(errorDesc)")
																self.alert(withTitle: "Something went wrong", description: errorDesc)
		})
		
	}
	
	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		return 100
	}
}


extension BankAccountsViewController: UITableViewDataSource {
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return bankAccounts.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "creditRewards") as! CreditCardRewardTableViewCell
		cell.transactionLabel.text = bankAccounts[indexPath.row].name
		cell.numberAndDateLabel.text = bankAccounts[indexPath.row].subtype
		cell.amountLabel.text = "\(bankAccounts[indexPath.row].balance)"
		cell.setBankImage()
		return cell
	}
}
