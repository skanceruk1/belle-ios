//
//  DetailsViewController.swift
//  Belle
//
//  Created by Steve on 4/11/19.
//  Copyright © 2019 Belle. All rights reserved.
//

import UIKit
import PhoneNumberKit

class DetailsViewController: UIViewController {

	@IBOutlet weak var titleLabel: UILabel!
	
	@IBOutlet weak var backButton: UIButton!
	@IBOutlet weak var nextButton: UIButton!
	
	@IBOutlet weak var decriptionTextView: UITextView!
	
	@IBOutlet weak var labelOne: UILabel!
	
	@IBOutlet weak var textFieldOne: UITextField!
	
	@IBOutlet weak var labelTwo: UILabel!
	
	@IBOutlet weak var textFieldTwo: UITextField!
	
	var page:Int?
	var user:User?
	var fromIntroFlow:Bool?
	
	override func viewDidLoad() {
        super.viewDidLoad()
		nextButton.layer.cornerRadius = 10
		nextButton.backgroundColor = UIColor.belleColor()
        // Do any additional setup after loading the view.
		
		styleLabels()
		
		if (page == 0) {
			self.titleLabel.text = "Thanks for joining Belle"
			self.decriptionTextView.text = "Can you tell us a little more about yourself? This helps us customize your account for optimal experience."
			
//			self.labelOne.text = "Phone Number:"
//			self.labelTwo.text = "Your Name:"
			
//			textFieldOne.keyboardType = .phonePad
			textFieldOne.keyboardType = .phonePad
			textFieldTwo.keyboardType = .alphabet
			
			if (Constants.TEST_MODE) {
				textFieldOne.text = "1018628080"
				textFieldTwo.text = "plaidPublicKey Test"
			}
			
		}
		else if (page == 1) {
//			phoneNumberKit = PhoneNumberKit()
			self.titleLabel.text = "Planning ahead for college."
			self.decriptionTextView.text = "Can you tell us a little more about your college planning? This helps us make smater recomendations about how to save."
			
//			self.labelOne.text = "Expected college start:"
//			self.labelTwo.text = "Number of children going to college:"
			
			textFieldOne.keyboardType = .numberPad
			textFieldTwo.keyboardType = .numberPad
			
			if (Constants.TEST_MODE) {
				textFieldOne.text = "1"
				textFieldTwo.text = "2050"
			}
			
		}
		
		styleLabels()
		
		let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
		view.addGestureRecognizer(tap)
    }
    

	func styleLabels() {
		
		let formattedString = NSMutableAttributedString()
		let formattedString2 = NSMutableAttributedString()
		
		if (page == 0) {
		
			formattedString
				.normal("Your phone number ")
				.bold(" (as a backup way to reach you): ")
			
			labelOne?.attributedText = formattedString
			
			
			formattedString2
				.normal("Your name: ")
				.bold(" (so we know what to call you):")
			labelTwo?.attributedText = formattedString2
			
		}
		else if (page == 1) {
			formattedString
				.normal("How many kids do you plan on sending to college?")
				//.bold(" (to keep things secure): ")

			labelOne?.attributedText = formattedString
 
			formattedString2
				.normal("When will the first one start college?")
				//.bold(" (just to be sure):")
			labelTwo?.attributedText = formattedString2
		}
	}
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

	@IBAction func nextButtonPressed(_ sender: Any) {
		if (page == 0) {
			if let phoneNumber = textFieldOne.text, let name = textFieldTwo.text {
				if phoneNumber.isEmpty || name.isEmpty {
					alert(withTitle: "More info needed", description: "Please fill out all fields")
					return
				}
				let viewController = self.storyboard?.instantiateViewController(withIdentifier: "detailsViewController") as! DetailsViewController
				viewController.page = self.page! + 1
				self.user?.phoneNumber = Int(phoneNumber)
				self.user?.name = name
				viewController.user = self.user
				viewController.fromIntroFlow = self.fromIntroFlow
				self.navigationController?.pushViewController(viewController, animated: true)
			}
			else {
				alert(withTitle: "More info needed", description: "Please fill out all fields")
				return
			}
		}
		else if (page == 1) {
			if let numberOfChildren = textFieldOne.text, let childStartYear = textFieldTwo.text {
				if childStartYear.isEmpty || numberOfChildren.isEmpty {
					alert(withTitle: "More info needed", description: "Please fill out all fields")
					return
				}
				let childStartYearNum = Int(childStartYear)
				if let num = childStartYearNum {
					if num < 2020 || num > 3000 {
						alert(withTitle: "College start year", description: "Please enter a valid college starting year")
						return
					}
				} 
				let viewController = self.storyboard?.instantiateViewController(withIdentifier: "connectViewController") as! ConnectViewController
				self.user?.childrenStartYear = childStartYearNum
				self.user?.childrenGradYear = (Int(childStartYear)! )
				self.user?.numberOfChildren = Int(numberOfChildren)
				viewController.user = self.user
				viewController.fromIntroFlow = self.fromIntroFlow
				self.navigationController?.pushViewController(viewController, animated: true)
			}
			else {
				alert(withTitle: "More info needed", description: "Please fill out all fields")
				return
			}
		}
	}
	
	@IBAction func backButtonPressed(_ sender: Any) {
		self.navigationController?.popViewController(animated: true)
	}
	
	@objc func dismissKeyboard() {
		//Causes the view (or one of its embedded text fields) to resign the first responder status.
		view.endEditing(true)
	}
	
}
