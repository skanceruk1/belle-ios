//
//  Slide.swift
//  Belle
//
//  Created by Steve on 4/7/19.
//  Copyright © 2019 Belle. All rights reserved.
//

import UIKit

class Slide: UIView {

	@IBOutlet weak var descriptionTextView: UITextView!
	
	@IBOutlet weak var imageView: UIImageView!
	/*
	// Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
