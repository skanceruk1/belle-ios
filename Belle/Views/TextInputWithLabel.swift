//
//  TextInputWithLabel.swift
//  Belle
//
//  Created by Steve on 4/13/19.
//  Copyright © 2019 Belle. All rights reserved.
//

import UIKit

class TextInputWithLabel: UIView {

	@IBOutlet weak var titleTextLabel: UILabel!
	
	@IBOutlet weak var inputTextField: UITextField!
	
	/*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

	func getInputText() -> String{
		if let text = inputTextField.text {
			return text
		}
		else {
			return ""
		}
	}
}
