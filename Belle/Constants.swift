//
//  Constants.swift
//  Belle
//
//  Created by Steve on 3/25/19.
//  Copyright © 2019 Belle. All rights reserved.
//

import Foundation

struct Constants {
	public static var BASE_URL_TEST = "http://0.0.0.0:8000"
	public static var BASE_URL_PRODUCTION = "http://belle-env.by6gxfwfmm.us-west-2.elasticbeanstalk.com"
	
	public static var PLAID_BASE_URL_SANDBOX = "https://sandbox.plaid.com"
	public static var PLAID_BASE_URL_DEVELOPMENT = "https://development.plaid.com"
	public static var PLAID_BASE_URL_PRODUCTION = "https://production.plaid.com"
//	public static var PLAID_API_SANDBOX = "public-sandbox-26a1443e-a1d1-4709-a2a1-0ca5346db554"
//	public static var PLAID_API_DEVELOPMENT = "public-sandbox-26a1443e-a1d1-4709-a2a1-0ca5346db554"
//	https://development.plaid.com
	public static var PLAID_CLIENT_ID = "5c8f30ed05b680001497eddd"
	
	public static var PLAID_PUBLIC_KEY = "6a8e0124c92cb3c9ba51c01a32a0e4"
	
	public static var PLAID_SECRET_SANDBOX = "c343f81f218a46da784b7ec664fc90"
	
	public static var PLAID_SECRET_DEVELOPMENT = "31541403145dee39aa94ac7abb2000"
	
//	public static var PLAID_ACCESS_TOKEN_SANDBOX = "access-sandbox-f95575b1-7448-4349-acff-ea4c6bb1fab5"
	// not sure if we need one for dev or its tied to the indivudual bank acocunt there
	
	public static var BANK_ACCOUNT_CHOSEN = "BANK_ACCOUNT_CHOSEN"
	public static var TEST_MODE = true // false
}
