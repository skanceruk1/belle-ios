//
//  CalculationDetailsViewController.swift
//  Belle
//
//  Created by Steve on 4/22/19.
//  Copyright © 2019 Belle. All rights reserved.
//

import UIKit

class CalculationViewController: UIViewController {

	@IBOutlet weak var backButton: UIButton!
	
	@IBOutlet weak var boldTextLabel: UILabel!
	
	@IBOutlet weak var textView: UITextView!
	
	@IBOutlet weak var gotItButton: UIButton!
	
	override func viewDidLoad() {
        super.viewDidLoad()

		boldTextLabel.text = "How does Belle forecast your 529 accounts’ value?"
		textView.text = "529 Savings Plan projections are based on the returns of the average savings plans. Our calculation assumes a 6% annual return compounded tax free over {X} years. \n\n For example, if you start with $1,000 in year one, you will have $1,000 * 1.06 = $1,060 at the end of year 1. If you start year 2 with $1,060, you will have $1,060 * 1.06 = $1,123.60 at the end of year 2."
		
		gotItButton.layer.cornerRadius = 10
		gotItButton.backgroundColor = UIColor.belleColor()
		gotItButton.titleLabel?.text = "Got it"
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

	@IBAction func backButtonPressed(_ sender: Any) {
		self.navigationController?.popViewController(animated: true)
	}
	
	@IBAction func gotItButtonPressed(_ sender: Any) {
		self.navigationController?.popViewController(animated: true)
	}
}
