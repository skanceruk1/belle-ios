//
//  ViewController.swift
//  Belle
//
//  Created by Steve on 3/9/19.
//  Copyright © 2019 Belle. All rights reserved.
//

import UIKit
import LinkKit
import SVProgressHUD

class LoginViewController: UIViewController {

	@IBAction func unwindToVCFromConnect(segue:UIStoryboardSegue) { }
	
	@IBAction func unwindToVCFromAccount(segue:UIStoryboardSegue) { }
	
	@IBOutlet weak var usernameTextField: UITextField!
	
	@IBOutlet weak var passwordTextField: UITextField!
	
	@IBOutlet weak var logInButton: UIButton!
	
	@IBOutlet weak var signUpButton: UIButton!
	
	override func awakeFromNib() {
		super.awakeFromNib()
		NotificationCenter.default.addObserver(self, selector: #selector(LoginViewController.didReceiveNotification(_:)), name: NSNotification.Name(rawValue: "PLDPlaidLinkSetupFinished"), object: nil)
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
		self.usernameTextField.text = "" //"johndoe@yahoo.com" // "" // "testaccount@yahoo.com" // "" //
		self.usernameTextField.placeholder = "Username"
		self.passwordTextField.text = "" // "testpassword" // "qwertylol" // "" //
		self.passwordTextField.placeholder = "Password"
		logInButton.layer.cornerRadius = 10
		logInButton.backgroundColor = UIColor.belleColor()
		signUpButton.setTitleColor(UIColor.belleColor(), for: .normal)
		let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
		view.addGestureRecognizer(tap)
	}

	override func viewWillDisappear(_ animated: Bool) {
		SVProgressHUD.dismiss()
	}
	
	@IBAction func loginButtonPressed(_ sender: Any) {
		view.endEditing(true)
		SVProgressHUD.show()
		BelleNetworkManager.shared.login(username: self.usernameTextField.text!,
										 password: self.passwordTextField.text!,
										 onSuccess: onSuccess,
										 onFailure: onFaliure)
	}
	
	@IBAction func signupButtonPressed(_ sender: Any) {
		let viewController = storyboard?.instantiateViewController(withIdentifier: "signUpViewController") as! SignUpViewController
		viewController.fromIntroFlow = false
		self.navigationController?.pushViewController(viewController, animated: true)
	}
	
	@IBAction func plaidTextButtonPressed(_ sender: Any) {
		
		let linkViewDelegate = self
		let linkViewController = PLKPlaidLinkViewController(delegate: linkViewDelegate)
		if (UI_USER_INTERFACE_IDIOM() == .pad) {
			linkViewController.modalPresentationStyle = .formSheet;
		}
		present(linkViewController, animated: true)
		
	}
	
	@objc func didReceiveNotification(_ notification: NSNotification) {
		if notification.name.rawValue == "PLDPlaidLinkSetupFinished" {
			NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
		}
	}

	func onSuccess() {
		
		SVProgressHUD.dismiss()
		
		BelleNetworkManager.shared.currentUser?.bankAccount = BankAccount()
		BelleNetworkManager.shared.currentUser?.creditCard = CreditCard() // credit card
		
		//BelleNetworkManager.shared.getBalance(onSuccess: {
		self.homeSegue()
		//}, onFailure: {
		//	errorDesc in
			//self.errorAlertWithDescription(description:"Something went wrong with connecting to Plaid")
		//})
	}
	
	func onFaliure(description:String) {
		SVProgressHUD.dismiss()
		errorAlertWithDescription(description: description, completion: {
			SVProgressHUD.dismiss()
		})
	}
	 
	func homeSegue() {
		self.performSegue(withIdentifier: "toHome", sender: self)
	}
	
	@objc func dismissKeyboard() {
		//Causes the view (or one of its embedded text fields) to resign the first responder status.
		view.endEditing(true)
	}
	
}

extension LoginViewController : PLKPlaidLinkViewDelegate {
	
	func linkViewController(_ linkViewController: PLKPlaidLinkViewController,
							didSucceedWithPublicToken publicToken: String,
							metadata: [String : Any]?) {
		
		// Handle success, e.g. by storing publicToken with your service
		print("Successfully linked account! npublicToken: \(publicToken)\nmetadata: (metadata ?? [:])")
		self.handleSuccessWithToken(publicToken, metadata: metadata)
		
	}
	
	func linkViewController(_ linkViewController: PLKPlaidLinkViewController,
							didExitWithError error: Error?,
							metadata: [String : Any]?) {
		//
		if let error = error {
			NSLog("Failed to link account due to: \(error.localizedDescription)\nmetadata: (metadata ?? [:])")
			self.handleError(error, metadata: metadata)
		}
		else {
			NSLog("Plaid link exited with metadata: \(metadata ?? [:])")
			self.handleExitWithMetadata(metadata)
		}
	}
	
	// optional
	func linkViewController(_ linkViewController: PLKPlaidLinkViewController,
							didHandleEvent event: String,
							metadata: [String : Any]?) {
		NSLog("Link event: (event)\nmetadata: (metadata ?? [:])")
	}
	
	func handleSuccessWithToken(_ publicToken:String, metadata:[String : Any]?) {
		dismiss(animated: true) {
			// Handle success, e.g. by storing publicToken with your service
			NSLog("Successfully linked account!\npublicToken: \(publicToken)\nmetadata: \(metadata ?? [:])")
			
			BelleNetworkManager.shared.setPublicToken(publicToken: publicToken)
//			BelleNetworkManager.shared.currentUser.
			
			if let accounts = metadata?["accounts"] as? [[String : Any]] {
				// print(String(describing: accounts))
				for account in accounts {
					print(String(describing: "   \(account)"))
					if let type = account["type"] as? String {
						if  type == "credit" {
							if let id = account["id"]  {
								BelleNetworkManager.shared.setCreditCardId(id: String(describing: id))
							}
						}
					}
				}
			}
			
			
			self.handleSuccessWithToken(publicToken, metadata: metadata)
			self.onSuccess()
		}
		
	}
	
	func handleError(_ error:Error, metadata: [String : Any]?) {
		dismiss(animated: true) {
			self.presentAlertViewWithTitle("Failure", message: "error: \(error.localizedDescription)\nmetadata: \(metadata ?? [:])")
		}
	}
	
	func handleExitWithMetadata(_ metadata: [String : Any]?) {
		dismiss(animated: true) {
			self.navigationController?.popViewController(animated: true)
			//			self.presentAlertViewWithTitle("Exit", message: "metadata: \(metadata ?? [:])")
		}
	}
	
	func presentAlertViewWithTitle(_ title: String, message: String) {
		let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
		let action = UIAlertAction(title: "OK", style: .default, handler: nil)
		alert.addAction(action)
		present(alert, animated: true, completion: nil)
	}
}
