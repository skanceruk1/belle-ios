//
//  ConnectViewController.swift
//  Belle
//
//  Created by Steve on 4/11/19.
//  Copyright © 2019 Belle. All rights reserved.
//

import UIKit
import LinkKit
import SVProgressHUD

class ConnectViewController: UIViewController {

	var user:User?
	var fromIntroFlow:Bool?
	
	@IBOutlet weak var backButton: UIButton!
	@IBOutlet weak var addCardButton: UIButton!
	
	@IBOutlet weak var testUserSwitch: UISwitch!
	
	
    override func viewDidLoad() {
        super.viewDidLoad() 
        // Do any additional setup after loading the view.
		let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
		view.addGestureRecognizer(tap)
		addCardButton.layer.cornerRadius = 10
		addCardButton.backgroundColor = UIColor.belleColor()
		
//		BelleNetworkManager.shared.setPlaidToSandbox()
	}
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
	
	@IBAction func backButtonPressed(_ sender: Any) {
		self.navigationController?.popViewController(animated: true)
	}
	
	@IBAction func addCardButton(_ sender: Any) {
		showPlaidLogin()
		// backToBeginning()
	}
	
	
	func showPlaidLogin() {
		
		let linkViewDelegate = self
		let linkViewController = PLKPlaidLinkViewController(delegate: linkViewDelegate)
		if (UI_USER_INTERFACE_IDIOM() == .pad) {
			linkViewController.modalPresentationStyle = .formSheet;
		}
		present(linkViewController, animated: true)
		
	}
	
	@objc func didReceiveNotification(_ notification: NSNotification) {
		if notification.name.rawValue == "PLDPlaidLinkSetupFinished" {
			NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
		}
	}
	
	func createUser() {
		
//		applyTestUserSwitch()
		
		//SVProgressHUD.show()
		BelleNetworkManager.shared.signup(username: self.user!.email!,
										  password1: self.user!.password!,
										  password2: self.user!.password!,
										  email: self.user!.email!,
										  collegeStartYear: self.user!.childrenStartYear!,
										  name: self.user!.name!,
										  numberOfChildren: self.user!.numberOfChildren!,
										  phoneNumber: self.user!.phoneNumber!,
										  onSuccess: onSuccess,
										  onFailure: alertWithDescription)
	}
	
	func onSuccess() {
		SVProgressHUD.dismiss()
		backToBeginning()
	}
	
	
	func alertWithDescription(description:String) {
		SVProgressHUD.dismiss()
		let alert = UIAlertController(title: "error", message: description, preferredStyle: .alert)
		let okAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
		alert.addAction(okAction)
		self.present(alert, animated: true, completion: nil)
	}
	
	@objc func dismissKeyboard() {
		//Causes the view (or one of its embedded text fields) to resign the first responder status.
		view.endEditing(true)
	}
	
	func backToBeginning() {
		if let introFlow = fromIntroFlow {
			if (introFlow) {
				performSegue(withIdentifier: "unwindSegueToIntroFlowFromConnect", sender: self)
			}
			else {
				performSegue(withIdentifier: "unwindSegueToBeginningFromConnect", sender: self)
			}
		}
	}
	
	@IBAction func testUserSwitchPressed(_ sender: Any) {
		
//		applyTestUserSwitch()
		
	}
	
	
	func applyTestUserSwitch() {
//		let isOn = testUserSwitch.isOn
//
//		// turn test user on
//		if isOn {
//			BelleNetworkManager.shared.setPlaidToSandbox()
//			self.user?.testUser = true
//		}
//			// turns test user off which is development
//		else {
//			BelleNetworkManager.shared.setPlaidToDevelopment()
//			self.user?.testUser = false
//		}
	}
}


extension ConnectViewController : PLKPlaidLinkViewDelegate {
	
	func linkViewController(_ linkViewController: PLKPlaidLinkViewController,
							didSucceedWithPublicToken publicToken: String,
							metadata: [String : Any]?) {
		
		// Handle success, e.g. by storing publicToken with your service
		print("Successfully linked account! npublicToken: \(publicToken)\nmetadata: (metadata ?? [:])")
		self.handleSuccessWithToken(publicToken, metadata: metadata)
		
	}
	
	func linkViewController(_ linkViewController: PLKPlaidLinkViewController,
							didExitWithError error: Error?,
							metadata: [String : Any]?) {
		//
		if let error = error {
			NSLog("Failed to link account due to: \(error.localizedDescription)\nmetadata: (metadata ?? [:])")
			self.handleError(error, metadata: metadata)
		}
		else {
			NSLog("Plaid link exited with metadata: \(metadata ?? [:])")
			self.handleExitWithMetadata(metadata)
		}
	}
	
	// optional
	func linkViewController(_ linkViewController: PLKPlaidLinkViewController,
							didHandleEvent event: String,
							metadata: [String : Any]?) {
		NSLog("Link event: (event)\nmetadata: (metadata ?? [:])")
	}
	
	func handleSuccessWithToken(_ publicToken:String, metadata:[String : Any]?) {
		dismiss(animated: true) {
			// Handle success, e.g. by storing publicToken with your service
			
			BelleNetworkManager.shared.setPublicToken(publicToken: publicToken)
			
			BelleNetworkManager.shared.exchangeTokens(onSuccess: {
				
				// self.handleSuccessWithToken(publicToken, metadata: metadata)
				NSLog("Successfully linked account!\npublicToken: \(publicToken)\nmetadata: \(metadata ?? [:])")
				
				self.createUser()
				
			}, onFailure: { errorDesc in
				
				self.alertWithDescription(description:"Something went wrong") 
				
			})
			
			/*
			if let accounts = metadata?["metadata"]  {
				print(String(describing: accounts))
				//				for account in accounts {
				//					print(String(describing: account))
				//				}
				
				if let accounts = metadata?["accounts"]  {
					print(String(describing: accounts))
					//				for account in accounts {
					//					print(String(describing: account))
					//				}
				}
				else {
					print(" a Something went wrong")
				}
				
			}
			else {
				print("Something went wrong")
			}
			*/
			 
			
		}
	}
	
	func handleError(_ error:Error, metadata: [String : Any]?) {
		dismiss(animated: true) {
			self.presentAlertViewWithTitle("Failure", message: "Failed to link account: \(error.localizedDescription)")
//			performSegue(withIdentifier: <#T##String#>, sender: <#T##Any?#>)
		}
	}
	
	func handleExitWithMetadata(_ metadata: [String : Any]?) {
		dismiss(animated: true) {
			self.navigationController?.popViewController(animated: true)
			//			self.presentAlertViewWithTitle("Exit", message: "metadata: \(metadata ?? [:])")
		}
	}
	
	func presentAlertViewWithTitle(_ title: String, message: String) {
		let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
		let action = UIAlertAction(title: "OK", style: .default, handler: nil)
		alert.addAction(action)
		present(alert, animated: true, completion: nil)
	}
	
	
}
