//
//  SignUpViewController.swift
//  Belle
//
//  Created by Steve on 3/18/19.
//  Copyright © 2019 Belle. All rights reserved.
//

import UIKit
import SVProgressHUD

class SignUpViewController: UIViewController {

	@IBOutlet weak var back: UIButton!
	
	
	@IBOutlet weak var emailLabel: UILabel!
	@IBOutlet weak var emailTextField: UITextField!
	
	
	@IBOutlet weak var passwordLabel: UILabel!
	@IBOutlet weak var passwordTextField: UITextField!
	
	
	@IBOutlet weak var confirmPasswordLabel: UILabel!
	@IBOutlet weak var confirmPasswordTextField: UITextField!
	
	@IBOutlet weak var signUpButton: UIButton!
	
	var fromIntroFlow:Bool?
	
	override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
		
		signUpButton.layer.cornerRadius = 10
		signUpButton.backgroundColor = UIColor.belleColor()
		
		let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard)) 
		view.addGestureRecognizer(tap)
		
		styleLabels()
		
		if (Constants.TEST_MODE) {
			emailTextField.text = "plaidPublicKey@test.com"
			passwordTextField.text = "qwertylol"
			confirmPasswordTextField.text = "qwertylol"
		}
		
    }
	
	func styleLabels() {
		var attributedText = NSMutableAttributedString()
//		let str1 = "Email "
//		let str2 = " (this will be you login ID):"
//		let attr1 = [NSAttributedString.Key.font: UIFont(name: "AvenirNext-Medium", size: 12)!]
//		let attr2 = [NSAttributedString.Key.foregroundColor: UIColor.red]
//		attributedText.append(NSAttributedString(string: str1, attributes: attr1))
//		attributedText.append(NSAttributedString(string: str2, attributes: attr2))
//
		attributedText
			.normal("Email ")
			.bold(" (this will be your login ID):")
		emailLabel?.attributedText = attributedText
		
		let formattedString = NSMutableAttributedString()
		formattedString
			.normal("Password ")
			.bold(" (to keep things secure): ")
		
//		var attributedText1 = NSMutableAttributedString()
//		let str3 = "Password "
//		let str4 = " (to keep things secure):"
//		let attr3 = [NSAttributedStringKey.foregroundColor: UIColor.blue]
//		let attr4 = [NSAttributedString.Key.foregroundColor: UIColor.gray]
//		attributedText1.append(NSAttributedString(string: str3, attributes: attr3))
//		attributedText1.append(NSAttributedString(string: str4, attributes: attr4))
		passwordLabel?.attributedText = formattedString
 
		let formattedString2 = NSMutableAttributedString()
		formattedString2
			.normal("Confirm password ")
			.bold(" (just to be sure):")
		confirmPasswordLabel?.attributedText = formattedString2
	}
	
	override func viewDidDisappear(_ animated: Bool) {
		SVProgressHUD.dismiss()
	}
	
	@IBAction func signupButtonPressed(_ sender: Any) {
		
		if (passwordTextField.text != confirmPasswordTextField.text) {
			alertWithDescription(description: "Password and confirm password fields do not match")
			return
		}
		
		
		
		if let email = emailTextField.text, let password = passwordTextField.text {
			
			if email.isEmpty || password.isEmpty {
				alert(withTitle: "More info needed", description: "Please fill out all fields")
				return
			}
			
			let viewController = self.storyboard?.instantiateViewController(withIdentifier: "detailsViewController") as! DetailsViewController
			viewController.page = 0
			viewController.user = User(username: email, email: email, password: password)
			viewController.fromIntroFlow = self.fromIntroFlow
			self.navigationController?.pushViewController(viewController, animated: true)
		}
		else {
			alert(withTitle: "More info needed", description: "Please fill out all fields")
			return
		}
//		SVProgressHUD.show()
//		BelleNetworkManager.shared.signup(username: self.usernameTextField.text!,
//										  password1: self.passwordTextField.text!,
//										  password2: self.confirmPasswordTextField.text!,
//										  email: self.emailTextField.text!,
//										  onSuccess: onSuccess,
//										  onFailure: alertWithDescription)
	}
	
	@IBAction func backPressed(_ sender: Any) {
		self.navigationController?.popViewController(animated: true)
	}
	
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

	func onSuccess() {
		SVProgressHUD.dismiss()
		let alert = UIAlertController(title: "Contrats!", message: "You have successfully created your account!", preferredStyle: .alert)
		let okAction = UIAlertAction(title: "OK", style: .default) { (action) in
			print("ok")
		}
		alert.addAction(okAction)
		self.present(alert, animated: true, completion: {
			print("lol")
		})
	}
	
	func alertWithDescription(description:String) {
		SVProgressHUD.dismiss()
		let alert = UIAlertController(title: "error", message: description, preferredStyle: .alert)
		let okAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
		alert.addAction(okAction)
		self.present(alert, animated: true, completion: nil)
	}
	
	func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
		self.view.endEditing(true)
	}
	
	@objc func dismissKeyboard() {
		//Causes the view (or one of its embedded text fields) to resign the first responder status.
		view.endEditing(true)
	}
}

extension SignUpViewController: UITextFieldDelegate {
	
	
	
}
