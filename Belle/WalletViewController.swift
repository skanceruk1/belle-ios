//
//  MoneyViewController.swift
//  Belle
//
//  Created by Steve on 3/14/19.
//  Copyright © 2019 Belle. All rights reserved.
//

import UIKit
import SVProgressHUD

class WalletViewController: UIViewController {

	@IBOutlet weak var moneyTableView: UITableView!
	
	@IBOutlet weak var transferSwitch: UISwitch!
	
	@IBOutlet weak var belleAccountAmoutLabel: UILabel!
	
	@IBOutlet weak var transferButton: UIButton!
	
	@IBOutlet weak var autoTransferLabel: UILabel!
	
	@IBOutlet weak var transferDateSchedueleLabel: UILabel!
	
	var transactions = [Transaction]()
	
	let formatter = NumberFormatter()
	
	
	
	
	override func viewDidLoad() {
        super.viewDidLoad()
		
		let nibOne = UINib(nibName: "MoneyTableViewCellOne", bundle: nil)
		moneyTableView.register(nibOne, forCellReuseIdentifier: "moneyCellOne")
		let nibTwo = UINib(nibName: "MoneyTableViewCellTwo", bundle: nil)
		moneyTableView.register(nibTwo, forCellReuseIdentifier: "moneyCellTwo")
		let nibThree = UINib(nibName: "MoneyTableViewCellThree", bundle: nil)
		moneyTableView.register(nibThree, forCellReuseIdentifier: "moneyCellThree")
		let nibCreditRewards = UINib(nibName: "CreditCardRewardTableViewCell", bundle: nil)
		moneyTableView.register(nibCreditRewards, forCellReuseIdentifier: "creditRewards")
		
		self.moneyTableView.dataSource = self
		self.moneyTableView.delegate = self
		self.moneyTableView.allowsSelection = false
		transferButton.layer.cornerRadius = 10
		transferButton.backgroundColor = UIColor.belleColor()
		transferSwitch.onTintColor = UIColor.belleColor()
		transferSwitch.tintColor = UIColor.belleColor()
		formatter.numberStyle = .currency
		formatter.maximumFractionDigits = 2
		
		NotificationCenter.default.addObserver(self, selector: #selector(updateCreditCardTable), name: NSNotification.Name(rawValue: "transactionsCompletedSuccesfully"), object: nil)
		NotificationCenter.default.addObserver(self, selector: #selector(addView), name: NSNotification.Name(rawValue: "transactionsCompletedUnSuccesfully"), object: nil)
		
		///
		///  network stuff
		///
		getCreditInfo()
		
		if (transferSwitch.isOn) {
			autoTransferLabel.text = "Automatic transfer is ON"
		}
		else {
			autoTransferLabel.text = "Automatic transfer is OFF"
		}
		
		let date = closestFifteenth()
		transferDateSchedueleLabel.text = "Next transfer schedueled for \(date)"
    }
    
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(true)
		updateCreditCardTable()
		if (self.belleAccountAmoutLabel.text == "") {
			self.belleAccountAmoutLabel.text == "Link 529 account for balance"
			getCreditInfo()
		}
	}
	 
	@objc func updateCreditCardTable() {
		self.moneyTableView.reloadData()
	}
	
	func getCreditInfo() {
		SVProgressHUD.show()
		BelleNetworkManager.shared.getCreditInfo(onSuccess: {
			
			print("\n\n\nTransactions: \n\n\n")
			SVProgressHUD.dismiss()
			BelleNetworkManager.shared.getCreditTransactions(onSuccess: { transactions in
				// update UI
				
				self.transactions = transactions
				let tranactionSum = transactions.map({$0.amountNum}).reduce(0, +)
				
				
				DispatchQueue.main.async {
					self.belleAccountAmoutLabel.text = self.formatter.string(from:NSNumber(value: tranactionSum))
					// "$\(String(describing: tranactionSum))"
					if (self.moneyTableView != nil) {
						self.moneyTableView.reloadData()
					}
				}
				
				/*
				if let balance = BelleNetworkManager.shared.currentUser?.creditCard?.current {
				self.belleAccountAmoutLabel.text =  "$\(String(describing: balance))"
				}
				else {
				self.belleAccountAmoutLabel.text =  " "
				}
				*/
			}, onFailure: { errorDesc in
				print(errorDesc)
				self.alert(withTitle: "Something went wrong", description: errorDesc)
			})
		}) { errorDesc in
			print(errorDesc)
			self.alert(withTitle: "Something went wrong", description: errorDesc)
		}
	}
	
	@objc func addView() {
		let label = UILabel(frame: CGRect(x: 0, y: self.moneyTableView.frame.origin.y, width: self.view.frame.width, height: 100))
		label.text = "No transactions found"
		self.moneyTableView.addSubview(label)
	}
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

	@IBAction func transferButtonPressed(_ sender: Any) {
		alert(withTitle: "Comming soon", description: "Feature comming soon!")
	}
	@IBAction func switchSwitched(_ sender: Any) {
		if (transferSwitch.isOn) {
			autoTransferLabel.text = "Automatic transfer is ON"
		}
		else {
			autoTransferLabel.text = "Automatic transfer is OFF"
		}
	}
}

extension WalletViewController: UITableViewDataSource {
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return transactions.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		
			let cell = moneyTableView.dequeueReusableCell(withIdentifier: "creditRewards") as! CreditCardRewardTableViewCell
			cell.transactionLabel.text = transactions[indexPath.row].name
			cell.amountLabel.text = formatter.string(from:NSNumber(value: transactions[indexPath.row].amountNum))
			// transactions[indexPath.row].amount.formatNumberToTwoDecimals()
			cell.numberAndDateLabel.text = "1234 - \(transactions[indexPath.row].date)"
			return cell
		
	}
	
	
}

extension WalletViewController: UITableViewDelegate {
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		print("lol row  \(indexPath.row)")
	}
	
	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		return 100
	}
	
}

struct Transaction {
	var name = ""
	var amount = ""
	var amountNum:Float = 0.0
	var date = ""
}
