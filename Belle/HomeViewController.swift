//
//  HomeViewController.swift
//  Belle
//
//  Created by Steve on 3/14/19.
//  Copyright © 2019 Belle. All rights reserved.
//

import UIKit
import FSLineChart
import LinkKit
import SVProgressHUD

class HomeViewController: UIViewController {

	@IBOutlet weak var titleLabel: UILabel!
	
	@IBOutlet weak var lineChart: FSLineChart!
	
	@IBOutlet weak var tableView: UITableView!
	
	var showLinkFiveTwentyNineView = false
	
	override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
		
		let nibOne = UINib(nibName: "HomeTableViewCellAccount", bundle: nil)
		tableView.register(nibOne, forCellReuseIdentifier: "homeTableViewCellAccount")
		let nibTwo = UINib(nibName: "HomeTableViewCellChart", bundle: nil)
		tableView.register(nibTwo, forCellReuseIdentifier: "homeTableViewCellChart")
		let nibThree = UINib(nibName: "HomeTableViewCellWays", bundle: nil)
		tableView.register(nibThree, forCellReuseIdentifier: "homeTableViewCellWays")
		let nibFour = UINib(nibName: "LinkFiveTwentyNineTableViewCell", bundle: nil)
		tableView.register(nibFour, forCellReuseIdentifier: "linkFiveTwentyNineCell")
		self.tableView.dataSource = self
		self.tableView.delegate = self
		self.tableView.allowsSelection = false
		
		self.titleLabel.text = "Hello, \(BelleNetworkManager.shared.currentUser?.name ?? "Test User")"
		
		
		getBankInfo()
		
		
		NotificationCenter.default.addObserver(self, selector: #selector(bankAccountChosen(_:)), name: NSNotification.Name(rawValue: Constants.BANK_ACCOUNT_CHOSEN), object: nil)
		
	}
	
	func getBankInfo() {
		SVProgressHUD.show()
		tableView.isUserInteractionEnabled = false
		self.view.isUserInteractionEnabled = false
		BelleNetworkManager.shared.getBankAccountsInfo(onSuccess: {
			
			print("\n\n\nTransactions: \n\n\n")
			self.tableView.isUserInteractionEnabled = true
			self.view.isUserInteractionEnabled = true
			SVProgressHUD.dismiss()
			
		}, onFailure: { errorDesc in
			print(errorDesc)
			self.tableView.isUserInteractionEnabled = true
			self.view.isUserInteractionEnabled = true
			SVProgressHUD.dismiss()
			self.alert(withTitle: "Something went wrong", description: errorDesc)
		})
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		if (BelleNetworkManager.shared.currentUser?.bankAccountItemID == nil || BelleNetworkManager.shared.currentUser?.bankAccountItemID == "" ) {
			showLinkFiveTwentyNineView = true //()
		}
		
		
		
	}
	
	func viewDidAppear() {
		
	}
	
	@objc private func bankAccountChosen(_ notification: Notification?) {
		showLinkFiveTwentyNineView = false
		
		if (tableView != nil) {
			tableView.reloadData()
		}
		
	}
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		
		if (!showLinkFiveTwentyNineView) {
			tableView.reloadData()
		}
	}
	
	func onBankAccountSelected(bankAccountId:String) {
		showLinkFiveTwentyNineView = false
		//SVProgressHUD.show()
		
		if(SVProgressHUD.isVisible()){
			SVProgressHUD.dismiss()
		}
		
		// tableView.setNeedsLayout()
		// tableView.delegate = self
		if(tableView != nil) {
			tableView.reloadData()
		}
	}
	
	/*
	func showLinkFiveTwentyNineView() {
		
	}
	*/

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

	func goToBankAccounts() {
		let vc = storyboard?.instantiateViewController(withIdentifier: "bankAccountsViewController") as! BankAccountsViewController
		vc.onBankAccountSelected = self.onBankAccountSelected
		self.navigationController?.pushViewController(vc, animated: true)
	}
	
}

extension HomeViewController: UITableViewDataSource {
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		if (showLinkFiveTwentyNineView) {
			return 1
		}
		return 3
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		if (indexPath.row == 0) {
			if (showLinkFiveTwentyNineView == true) {
				let cell = tableView.dequeueReusableCell(withIdentifier: "linkFiveTwentyNineCell") as! LinkFiveTwentyNineTableViewCell
				cell.onHomePage = true
				cell.updateText()
				cell.goToLink = {
					/*
					let linkViewDelegate = self
					let linkViewController = PLKPlaidLinkViewController(delegate: linkViewDelegate)
					if (UI_USER_INTERFACE_IDIOM() == .pad) {
						linkViewController.modalPresentationStyle = .formSheet;
					}
					self.present(linkViewController, animated: true)
					*/
					self.goToBankAccounts() 
				}
				return cell
			}
			let cell = tableView.dequeueReusableCell(withIdentifier: "homeTableViewCellAccount") as! HomeTableViewCellAccount
			return cell
		}
		else if (indexPath.row == 1) {
			let cell = tableView.dequeueReusableCell(withIdentifier: "homeTableViewCellChart") as! HomeTableViewCellChart
			cell.goToDetails = {
				if let viewController = self.storyboard?.instantiateViewController(withIdentifier: "calculationViewController") as? CalculationViewController {
					self.navigationController?.pushViewController(viewController, animated: true)
				}
			}
			return cell
		}
		else {
			let cell = tableView.dequeueReusableCell(withIdentifier: "homeTableViewCellWays") as! HomeTableViewCellWays
			cell.goToContribute = {
				if let viewController = self.storyboard?.instantiateViewController(withIdentifier: "contributorsViewController") as? ContributorsViewController{
					self.navigationController?.pushViewController(viewController, animated: true)
				}
			}
			cell.goToEnroll = {
				if let viewController = self.storyboard?.instantiateViewController(withIdentifier: "offersViewController") as? OffersViewController {
					viewController.toggleEnabled = true
					 self.navigationController?.pushViewController(viewController, animated: true)
				}
			}
			return cell
		}
	}
	
	
}

extension HomeViewController: UITableViewDelegate {
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		print("lol row  \(indexPath.row)")
	}
	
	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		if (showLinkFiveTwentyNineView) {
			if (indexPath.row == 0) {
				return 330
			}else {
				return 600
			}
		}
		
		if (indexPath.row == 0) {
			return 140
		}
		if (indexPath.row == 1) {
			return 390
		}
		else if (indexPath.row == 2) {
			return 155
		}
		
		return 155
	}
	
}


extension HomeViewController : PLKPlaidLinkViewDelegate {
	
	func linkViewController(_ linkViewController: PLKPlaidLinkViewController,
							didSucceedWithPublicToken publicToken: String,
							metadata: [String : Any]?) {
		
		// Handle success, e.g. by storing publicToken with your service
		print("Successfully linked account! npublicToken: \(publicToken)\nmetadata: (metadata ?? [:])")
		self.handleSuccessWithToken(publicToken, metadata: metadata)
		
	}
	
	func linkViewController(_ linkViewController: PLKPlaidLinkViewController,
							didExitWithError error: Error?,
							metadata: [String : Any]?) {
		//
		if let error = error {
			NSLog("Failed to link account due to: \(error.localizedDescription)\nmetadata: (metadata ?? [:])")
			self.handleError(error, metadata: metadata)
		}
		else {
			NSLog("Plaid link exited with metadata: \(metadata ?? [:])")
			self.handleExitWithMetadata(metadata)
		}
	}
	
	// optional
	func linkViewController(_ linkViewController: PLKPlaidLinkViewController,
							didHandleEvent event: String,
							metadata: [String : Any]?) {
		NSLog("Link event: (event)\nmetadata: (metadata ?? [:])")
	}
	
	func handleSuccessWithToken(_ publicToken:String, metadata:[String : Any]?) {
		dismiss(animated: true) {
			
			// Handle success, e.g. by storing publicToken with your service
			
			print(metadata)
			
			self.goToBankAccounts()
			
			/*
			BelleNetworkManager.shared.setPublicTokenBank(publicToken: publicToken)
			
			BelleNetworkManager.shared.exchangeTokensBank(onSuccess: {
				
				// self.handleSuccessWithToken(publicToken, metadata: metadata)
				NSLog("Successfully linked account!\npublicToken: \(publicToken)\nmetadata: \(metadata ?? [:])")
				
				//self.createUser()
				BelleNetworkManager.shared.getBankInfo(onSuccess: {
					
				}, onFailure: { error in
					
				})
				
			}, onFailure: { errorDesc in
				
				self.errorAlertWithDescription(description:"Something went wrong")
				
			})
			*/
			
			/*
			BelleNetworkManager.shared.setPublicToken(publicToken: publicToken)
			
			BelleNetworkManager.shared.exchangeTokens(onSuccess: {
				
				// self.handleSuccessWithToken(publicToken, metadata: metadata)
				NSLog("Successfully linked account!\npublicToken: \(publicToken)\nmetadata: \(metadata ?? [:])")
				
				//self.createUser()
				
			}, onFailure: { errorDesc in
				
				self.alertWithDescription(description:"Something went wrong")
				
			})
			*/
			/*
			if let accounts = metadata?["metadata"]  {
			print(String(describing: accounts))
			//				for account in accounts {
			//					print(String(describing: account))
			//				}
			
			if let accounts = metadata?["accounts"]  {
			print(String(describing: accounts))
			//				for account in accounts {
			//					print(String(describing: account))
			//				}
			}
			else {
			print(" a Something went wrong")
			}
			
			}
			else {
			print("Something went wrong")
			}
			*/
			
			
		}
	}
	
	func handleError(_ error:Error, metadata: [String : Any]?) {
		dismiss(animated: true) {
			self.presentAlertViewWithTitle("Failure", message: "error: \(error.localizedDescription)\nmetadata: \(metadata ?? [:])")
			//			performSegue(withIdentifier: <#T##String#>, sender: <#T##Any?#>)
		}
	}
	
	func handleExitWithMetadata(_ metadata: [String : Any]?) {
		dismiss(animated: true) {
			self.navigationController?.popViewController(animated: true)
			//			self.presentAlertViewWithTitle("Exit", message: "metadata: \(metadata ?? [:])")
		}
	}
	
	func presentAlertViewWithTitle(_ title: String, message: String) {
		let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
		let action = UIAlertAction(title: "OK", style: .default, handler: nil)
		alert.addAction(action)
		present(alert, animated: true, completion: nil)
	}
}

/*
struct BankAccount {
	var name = ""
	var balance = ""
	var balanceNum:Float = 0.0
	var date = ""
	var type = ""
}
*/
