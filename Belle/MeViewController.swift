//
//  MeViewController.swift
//  Belle
//
//  Created by Steve on 3/14/19.
//  Copyright © 2019 Belle. All rights reserved.
//

import UIKit

class MeViewController: UIViewController {

	@IBOutlet weak var meTableView: UITableView!
	
	override func viewDidLoad() {
        super.viewDidLoad()

		let offerNib = UINib(nibName: "MeTableViewCell", bundle: nil)
		meTableView.register(offerNib, forCellReuseIdentifier: "meCell")
		meTableView.delegate = self
		meTableView.dataSource = self
//		meTableView.rowHeight = UITableView.automaticDimension
		meTableView.estimatedRowHeight = 110
        // Do any additional setup after loading the view.
//		meTableView.allowsSelection = false
    }
    
	override func viewWillAppear(_ animated: Bool) {
		self.navigationController?.isNavigationBarHidden = true
		if let index = self.meTableView.indexPathForSelectedRow{
			self.meTableView.deselectRow(at: index, animated: true)
		}
	}
	
    /*
	
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


extension MeViewController: UITableViewDataSource {
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return 3
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let meCell = meTableView.dequeueReusableCell(withIdentifier: "meCell", for: indexPath) as! MeTableViewCell
		if (indexPath.row == 0) {
			meCell.setImage(name: "iconMe")
			meCell.setTitle(title: "Account details")
		}
		else if (indexPath.row == 1) {
			meCell.setImage(name: "iconCreditCard")
			meCell.setTitle(title: "Credit cards & 529")
		}
		else {
			meCell.setImage(name: "iconPeople")
			meCell.setTitle(title: "529 contributors")
		}
		return meCell
	}
	
}


extension MeViewController: UITableViewDelegate {
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		
		if (indexPath.row == 0) {
			self.performSegue(withIdentifier: "toAccount", sender: self)
		}
		else if (indexPath.row == 1) {
			self.performSegue(withIdentifier: "toFinance", sender: self)
		}
		else if (indexPath.row == 2) {
			self.performSegue(withIdentifier: "toContributors", sender: self)
		}
		
	}
	
	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		return 100
	}
	
}
