//
//  Extensions.swift
//  Belle
//
//  Created by Steve on 4/7/19.
//  Copyright © 2019 Belle. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
	static func belleBlueColor() -> UIColor {
		let blue = UIColor(displayP3Red: 0.4, green: 0.667, blue: 1.0, alpha: 1.0)
		return blue
	}
	static func bellePurpleColor() -> UIColor {
		let blue = UIColor(displayP3Red: 0.302, green: 0.075, blue: 0.820, alpha: 1.0)
		return blue
	}
	static func bellePurpleColorLight() -> UIColor {
		let blue = UIColor(displayP3Red: 0.930, green: 0.906, blue: 0.980, alpha: 0.6)
		return blue
	}
	static func belleColor() -> UIColor { 
		return bellePurpleColor()
	}
}

extension NSMutableAttributedString {
	@discardableResult func bold(_ text: String) -> NSMutableAttributedString {
		let attrs: [NSAttributedString.Key: Any] = [.font: UIFont(name: "AvenirNext-Medium", size: 16)!]
		let boldString = NSMutableAttributedString(string:text, attributes: attrs)
		append(boldString)
		
		return self
	}
	
	@discardableResult func normal(_ text: String) -> NSMutableAttributedString {
		let normal = NSAttributedString(string: text)
		append(normal)
		
		return self
	}
}


extension UIViewController {
	
	func alert(withTitle:String, description:String) {
		let alert = UIAlertController(title: withTitle, message: description, preferredStyle: .alert)
		let okAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
		alert.addAction(okAction)
		self.present(alert, animated: true, completion: nil)
	}
	
	func errorAlertWithDescription(description:String) { 
		let alert = UIAlertController(title: "Error", message: description, preferredStyle: .alert)
		let okAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
		alert.addAction(okAction)
		self.present(alert, animated: true, completion: nil)
	}
	
	func errorAlertWithDescription(description:String, completion: @escaping ()->()) {
		let alert = UIAlertController(title: "Error", message: description, preferredStyle: .alert)
		let okAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
		alert.addAction(okAction)
		self.present(alert, animated: true, completion: {
			completion()
		})
	}
	
}

func todayDateAsString() -> String {
	let date = Date()
	let dateFormatter = DateFormatter()
	dateFormatter.dateFormat = "MMM d, yyyy"
	let dateString = dateFormatter.string(from: date)
	return dateString
}

func closestFifteenth() -> String {
	
	let date = Date()
	let dateFormatter = DateFormatter()
	dateFormatter.dateFormat = "MMM d, yyyy"
	
	let calendar = NSCalendar.current
	let todayComponents = calendar.dateComponents([.year,.month,.day], from: date )
	
	let totalComponents = NSDateComponents()
	
	if (todayComponents.day! < 15) {
		totalComponents.year = todayComponents.year!
		totalComponents.month = todayComponents.month!
		totalComponents.day = 15
	}
	else if (todayComponents.month! < 11) {
		totalComponents.year = todayComponents.year!
		totalComponents.month = todayComponents.month! + 1
		totalComponents.day = 15
	}
	else { // new year
		totalComponents.year = todayComponents.year! + 1
		totalComponents.month =  1
		totalComponents.day = 15
	}
	
	if let updatedDate = calendar.date(from: totalComponents as DateComponents) {
		let dateString = dateFormatter.string(from: updatedDate)
		return dateString
	}
	
	let dateString = dateFormatter.string(from: date)
	return dateString
	
}

extension String {
	func formatNumberToTwoDecimals() -> String{
		var split = self.components(separatedBy: ".")
		if split.count == 0 || split.count == 1{
			return self
		}
		var formatted = split[0]
		var dec = split[1]
		var oneDec = ""
		var twoDec = ""
		if (dec.count > 1) {
			oneDec = String(dec.removeFirst())
			if (dec.count > 2) {
				twoDec = String(dec.removeFirst())
			}
			else {
				twoDec = "0"
			}
		}
		else {
			oneDec = "0"
			twoDec = "0"
		}
		
		formatted = "\(formatted).\(oneDec)\(twoDec)"
		return formatted
	}
}
