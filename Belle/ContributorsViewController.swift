//
//  ContributorsViewController.swift
//  Belle
//
//  Created by Steve on 4/11/19.
//  Copyright © 2019 Belle. All rights reserved.
//

import UIKit

class ContributorsViewController: UIViewController {

	@IBOutlet weak var backButtonPressed: UIButton!
	
	@IBOutlet weak var contribImageView: UIImageView!
	
	@IBOutlet weak var descriptiontextView: UITextView!
	
	@IBOutlet weak var inviteLabel: UILabel!
	
	@IBOutlet weak var inviteTextField: UITextField!
	
	@IBOutlet weak var inviteButton: UIButton!
	
	override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
		inviteButton.layer.cornerRadius = 10
		inviteButton.backgroundColor = UIColor.belleColor()
		descriptiontextView.text = "It's easier to save together! \n\n When your friends and family join Belle, they can contribute funds directly to your 529 account."
		let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
		view.addGestureRecognizer(tap)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

	@IBAction func backButtonPressed(_ sender: Any) {
		self.navigationController?.popViewController(animated: true)
	}
	
	@IBAction func nextButtonPressed(_ sender: Any) {
		alert(withTitle: "Coming soon", description: "We are not quite ready to manage external 529 conributions yet. \n\n Don't worry though, we'll send all your invites as soon as we support external contributions.")
	}
	
	@objc func dismissKeyboard() {
		//Causes the view (or one of its embedded text fields) to resign the first responder status.
		view.endEditing(true)
	}
}
