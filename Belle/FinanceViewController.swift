//
//  FinanceViewController.swift
//  Belle
//
//  Created by Steve on 4/11/19.
//  Copyright © 2019 Belle. All rights reserved.
//

import UIKit

class FinanceViewController: UIViewController {

	@IBOutlet weak var backButton: UIButton!
	
	@IBOutlet weak var addCreditCardButton: UIButton!
	
	@IBOutlet weak var registerAccountButton: UIButton!
	
	@IBOutlet weak var tableView: UITableView!
	
	override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
		self.addCreditCardButton.layer.cornerRadius = 10
		self.addCreditCardButton.backgroundColor = UIColor.belleColor()
		self.registerAccountButton.layer.cornerRadius = 10
		self.registerAccountButton.backgroundColor = UIColor.belleColor()
		
		let nib = UINib(nibName: "MoneyTableViewCellThree", bundle: nil)
		tableView.register(nib, forCellReuseIdentifier: "moneyCellThree")
		self.tableView.dataSource = self
		self.tableView.delegate = self
		self.tableView.allowsSelection = false
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

	@IBAction func backButtonPressed(_ sender: Any) {
		self.navigationController?.popViewController(animated: true)
	}
	
	@IBAction func addCreditCardButtonPressed(_ sender: Any) {
		alert(withTitle: "Comming soon", description: "Feature comming soon!")
	}
	
	@IBAction func registerFiveTwoNinePressed(_ sender: Any) {
		alert(withTitle: "Comming soon", description: "Feature comming soon!")
	}
	
}

extension FinanceViewController: UITableViewDataSource {
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return 1
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		
		let cell = tableView.dequeueReusableCell(withIdentifier: "moneyCellThree") as! MoneyTableViewCellThree
		return cell
		
	}
	
	
}

extension FinanceViewController: UITableViewDelegate {
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		print("lol row  \(indexPath.row)")
	}
	
	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		
		return 80
	}
	
}
