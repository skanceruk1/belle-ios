//
//  LastViewController.swift
//  Belle
//
//  Created by Steve on 3/9/19.
//  Copyright © 2019 Belle. All rights reserved.
//

import UIKit

class AccountViewController: UIViewController {

	@IBOutlet weak var signOutButton: UIButton!
	
	override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

	@IBAction func signOutPressed(_ sender: Any) {
		performSegue(withIdentifier: "unwindSegueToBeginning", sender: self)
	}
	/*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
