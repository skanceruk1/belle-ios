//
//  HomeTableViewCellOne.swift
//  Belle
//
//  Created by Steve on 4/7/19.
//  Copyright © 2019 Belle. All rights reserved.
//

import UIKit

class HomeTableViewCellAccount: UITableViewCell {

	@IBOutlet weak var titleLabel: UILabel! 
	@IBOutlet weak var accountValueLabel: UILabel!
	@IBOutlet weak var dateLabel: UILabel!
	
	override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
		
		// accountValueLabel.textColor = UIColor.belleColor()
		
		if let balance = BelleNetworkManager.shared.currentUser?.bankAccount?.balanceNum {
			let formatter = NumberFormatter()
			formatter.numberStyle = .currency
			formatter.maximumFractionDigits = 2
			if let balanceFromatted = formatter.string(for: NSNumber(value: balance)) {
				accountValueLabel.text =  "\(String(describing: balanceFromatted))"
			}
			else {
				accountValueLabel.text = "$\(String(describing:balance))"
			}
			let dateString = todayDateAsString()
			dateLabel.text = "Updated \(dateString)"
		}
		else {
			accountValueLabel.text =  " "
		}
		
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
