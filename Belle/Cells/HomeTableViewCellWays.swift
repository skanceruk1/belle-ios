//
//  HomeTableViewCellEnroll.swift
//  Belle
//
//  Created by Steve on 4/7/19.
//  Copyright © 2019 Belle. All rights reserved.
//

import UIKit

class HomeTableViewCellWays
: UITableViewCell {

	@IBOutlet weak var titleLabel: UILabel!
	
	@IBOutlet weak var buttonOne: UIButton!
	
	@IBOutlet weak var buttonTwo: UIButton!
	
	var goToContribute: (()->())?
	var goToEnroll: (()->())?
	
	override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
		buttonOne.layer.cornerRadius = 10
		buttonTwo.layer.cornerRadius = 10
		buttonOne.backgroundColor = UIColor.belleColor()
		buttonTwo.backgroundColor = UIColor.belleColor()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
	@IBAction func inviteFriendsButtonPressed(_ sender: Any) {
		if let go = goToContribute {
			go()
		}
	}
	
	@IBAction func enrollOffersButtonPressed(_ sender: Any) {
		if let go = goToEnroll {
			go()
		}
	}
	
}
