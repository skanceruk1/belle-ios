//
//  TitleTableViewCell.swift
//  Belle
//
//  Created by Steve on 5/3/19.
//  Copyright © 2019 Belle. All rights reserved.
//

import UIKit

class TitleTableViewCell: UITableViewCell {

	@IBOutlet weak var titleLabel: UILabel!
	
	override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
