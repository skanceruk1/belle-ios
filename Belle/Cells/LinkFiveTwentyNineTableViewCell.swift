//
//  LinkFiveTwentyNineTableViewCell.swift
//  Belle
//
//  Created by Steve on 4/30/19.
//  Copyright © 2019 Belle. All rights reserved.
//

import UIKit

class LinkFiveTwentyNineTableViewCell: UITableViewCell {

	var onHomePage = false
	
	@IBOutlet weak var descTextView: UITextView!
	
	@IBOutlet weak var linkButton: UIButton!
	
	var goToLink: (()->())?
	
	override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code 
		linkButton.layer.cornerRadius = 10
		linkButton.backgroundColor = UIColor.belleColor()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
	@IBAction func linkButtonPressed(_ sender: Any) {
		self.goToLink!()
	}
	
	func updateText() {
		if (onHomePage) {
			descTextView.text = "Your account is created and your credit card is already earning you rewards! \n\n\nThe next step is to register an existing 529 account, or create a new one."
		}
		else {
			descTextView.text = "You can transfer your wallet funds directly into a 529 account."
		}
	}
}
