//
//  OffersTableViewCell.swift
//  Belle
//
//  Created by Steve on 3/14/19.
//  Copyright © 2019 Belle. All rights reserved.
//

import UIKit

class OffersTableViewCell: UITableViewCell {

	@IBOutlet weak var backGroundView: UIView!
	
	@IBOutlet weak var offerLogoImage: UIImageView!
	
	@IBOutlet weak var offerCompanyLabel: UILabel!
	
	@IBOutlet weak var offerCompanyDescription: UILabel!
	
	@IBOutlet weak var arrowImageView: UIImageView!
	@IBOutlet weak var offerAmountLabel: UILabel!
	
	override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
		backGroundView.layer.cornerRadius = 10
		backGroundView.layer.borderWidth = 1
		backGroundView.layer.borderColor = UIColor(red:222/255, green:225/255, blue:227/255, alpha: 1).cgColor
		backGroundView.backgroundColor = UIColor.white
		
		offerCompanyLabel.textColor = UIColor.belleColor()
		arrowImageView.image = arrowImageView.image?.withRenderingMode(.alwaysTemplate)
		arrowImageView.tintColor = UIColor.belleColor()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
	
//	func noir(originalImageView: UIImageView) {
//		var context = CIContext(options: nil)
//		let currentFilter = CIFilter(name: "CIPhotoEffectNoir")
//		currentFilter!.setValue(CIImage(image: originalImageView.image!), forKey: kCIInputImageKey)
//		let output = currentFilter!.outputImage
//		let cgimg = context.createCGImage(output!,from: output!.extent)
//		let processedImage = UIImage(cgImage: cgimg!)
//		originalImageView.image = processedImage
//	}
//    
}
