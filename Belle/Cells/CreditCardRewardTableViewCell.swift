//
//  CreditCardRewardTableViewCell.swift
//  Belle
//
//  Created by Steve on 4/22/19.
//  Copyright © 2019 Belle. All rights reserved.
//

import UIKit

class CreditCardRewardTableViewCell: UITableViewCell {

	@IBOutlet weak var backGroundView: UIView!
	@IBOutlet weak var transactionLabel: UILabel!
	@IBOutlet weak var numberAndDateLabel: UILabel!
	@IBOutlet weak var creditCardImageView: UIImageView!
	@IBOutlet weak var amountLabel: UILabel!
	@IBOutlet weak var arrowLabel: UIImageView!
	
	
	override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
		
		backGroundView.layer.cornerRadius = 10
		backGroundView.layer.borderWidth = 1
		backGroundView.backgroundColor = UIColor.white
		backGroundView.layer.borderColor = UIColor(red:222/255, green:225/255, blue:227/255, alpha: 1).cgColor
		transactionLabel.textColor = UIColor.belleColor()
		arrowLabel.image = arrowLabel.image?.withRenderingMode(.alwaysTemplate)
		arrowLabel.tintColor = UIColor.belleColor()
		arrowLabel.isHidden = true
    }

	func setBankImage() {
		creditCardImageView.image = UIImage(named: "piggyBankIcon")
		arrowLabel.isHidden = false
	}
	
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
