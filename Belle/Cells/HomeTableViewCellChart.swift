//
//  HomeTableViewCellChart.swift
//  Belle
//
//  Created by Steve on 4/7/19.
//  Copyright © 2019 Belle. All rights reserved.
//

import UIKit
import FSLineChart

class HomeTableViewCellChart: UITableViewCell {

	@IBOutlet weak var chartTitleLabel: UILabel!
	
	@IBOutlet weak var lineChart: FSLineChart!
	
	@IBOutlet weak var chartDescriptionLabel: UILabel!
	
	@IBOutlet weak var calculatedButton: UIButton!
	
	@IBOutlet weak var descriptionLabel: UILabel!
	
	@IBOutlet weak var todayLabel: UILabel!
	
	@IBOutlet weak var collegeLabel: UILabel!
	
	@IBOutlet weak var beginAmmountLabel: UILabel!
	
	@IBOutlet weak var endAmmountLabel: UILabel!
	
	
	var goToDetails: (()->())?
	var currentYear = 2019
	
	override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
		var data = [Float]()
		var year = 2019
		
		let formatter = NumberFormatter()
		formatter.numberStyle = .currency
		formatter.maximumFractionDigits = 0
		
		if let balanceNum = BelleNetworkManager.shared.currentUser?.bankAccount?.balanceNum {
			
			let balanceStr = formatter.string(from:NSNumber(value: balanceNum)) //"\(Int(balanceNum))" //.formatNumberToTwoDecimals()
			beginAmmountLabel.text = balanceStr //"$\(balanceStr)"
			beginAmmountLabel.textColor = UIColor.belleColor()
			
			data.append(balanceNum)
			
			year = (BelleNetworkManager.shared.currentUser?.getCollegeStartInt())!
			if (balanceNum > 0) {
			
				if year > 2019 {
					for i in 1...(year - currentYear) {
						data.append(Float(Double(data[i-1])*1.06))
					}
				}
				else {
					for i in 1...10 {
						data.append(Float(Double(data[i-1])*1.06))
					}
				}
			}
			
			let endAmmountStr = formatter.string(from:NSNumber(value: data[data.count-1]))
			//  "\(Int())"
			endAmmountLabel.text = endAmmountStr // "$\(endAmmountStr)"
			endAmmountLabel.textColor = UIColor.belleColor()
			
			collegeLabel.text = "\(year)"

		}
		else {
			var b = BelleNetworkManager.shared.currentUser?.bankAccount?.balanceNum
		}
		
		lineChart.gridStep = 5
		lineChart.verticalGridStep = 2
		lineChart.horizontalGridStep = 5
//		lineChart.labelForIndex = { "\(2019 + $0)" }
//		lineChart.labelForIndex[0] = 2019
//		lineChart.labelForValue = { "$\($0)" }
		
		lineChart.color =  UIColor.belleColor()
		lineChart.fillColor = UIColor.bellePurpleColorLight()
//		lineChart.dataPointColor = UIColor.belleColor()
//		lineChart.tintColor = UIColor.green //belleColor()
		
		lineChart.setChartData(data)
		
		lineChart.backgroundColor = UIColor.clear 
		
		calculatedButton.setTitleColor(UIColor.belleColor(), for: .normal)  
		if let worth = data.last {
			if let worthFormatted = formatter.string(from:NSNumber(value: worth)) {
				// "\(worth)".formatNumberToTwoDecimals()
			descriptionLabel.text = "Your account could be worth \(String(describing: worthFormatted)) by the time your kid starts college in \(year)"
			}
		}
		else {
			descriptionLabel.text = "You currently have a balance of $0. However there is still time to save before your kid starts college in \(year)!"
		}
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
	@IBAction func calculatedButtonPressed(_ sender: Any) {
		if let go = goToDetails {
			go()
		}
	}
}
