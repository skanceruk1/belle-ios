//
//  MoneyTableViewCellTwo.swift
//  Belle
//
//  Created by Steve on 3/17/19.
//  Copyright © 2019 Belle. All rights reserved.
//

import UIKit

class MoneyTableViewCellTwo: UITableViewCell {

	@IBOutlet weak var backGroundView: UIView!
	
	override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
		self.backGroundView.layer.cornerRadius = 10
		self.backGroundView.layer.borderWidth = 1
		self.backGroundView.layer.borderColor = UIColor(red:222/255, green:225/255, blue:227/255, alpha: 1).cgColor
		self.backGroundView.backgroundColor = UIColor.white
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
