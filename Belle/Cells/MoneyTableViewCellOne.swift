//
//  MoneyTableViewCellOne.swift
//  Belle
//
//  Created by Steve on 3/17/19.
//  Copyright © 2019 Belle. All rights reserved.
//

import UIKit
import FSLineChart

class MoneyTableViewCellOne: UITableViewCell {

	@IBOutlet weak var backGroundView: UIView!
	
	@IBOutlet weak var lineChart: FSLineChart!
	
	override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
		self.backGroundView.layer.cornerRadius = 10
		var data = [Int]()
		
		data.append(0)
		
		for i in 1...3 {
			data.append(data[i-1] + 10)
		}
		for i in 4...6 {
			data.append(data[i-1])
		}
		for i in 7...8 {
			data.append(data[i-1] + 20)
		}
		for i in 9...10 {
			data.append(data[i-1])
		}
		
		lineChart.verticalGridStep = 5
		lineChart.horizontalGridStep = 9
		lineChart.labelForIndex = { "\($0)" }
		lineChart.labelForValue = { "$\($0)" }
		lineChart.setChartData(data)
		lineChart.backgroundColor = UIColor.clear
		
		self.backGroundView.layer.borderWidth = 1
		self.backGroundView.layer.borderColor = UIColor(red:222/255, green:225/255, blue:227/255, alpha: 1).cgColor
		self.backGroundView.backgroundColor = UIColor.white
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
