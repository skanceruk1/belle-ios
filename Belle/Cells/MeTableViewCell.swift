//
//  MeTableViewCell.swift
//  Belle
//
//  Created by Steve on 3/14/19.
//  Copyright © 2019 Belle. All rights reserved.
//

import UIKit

class MeTableViewCell: UITableViewCell {

	
	@IBOutlet weak var titleLabel: UILabel!
	@IBOutlet weak var backGroundView: UIView!
	
	@IBOutlet weak var iconImageView: UIImageView!
	
	@IBOutlet weak var arrowImageView: UIImageView!
	
	override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
		self.backGroundView.layer.cornerRadius = 10
		self.backGroundView.layer.borderWidth = 1
		self.backGroundView.layer.borderColor = UIColor(red:222/255, green:225/255, blue:227/255, alpha: 1).cgColor
		self.backGroundView.backgroundColor = UIColor.white
		
		
		titleLabel.textColor = UIColor.belleColor()
		
		arrowImageView.image = arrowImageView.image?.withRenderingMode(.alwaysTemplate)
		arrowImageView.tintColor = UIColor.belleColor()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
	
	func setTitle(title: String) {
		titleLabel.text = title
	}
	
	func setImage(name: String) {
		iconImageView?.image = UIImage(named: name)
		iconImageView.image = iconImageView.image?.withRenderingMode(.alwaysTemplate)
		iconImageView.tintColor = UIColor.belleColor() 
	}
}
